# Pysimmods

This projects contains a collection of (python) simulation models for a variety of energy generation and consumption units. Most of the models were originally developed in the context of the research project iHEM (intelligent Home Energy Management) or other projects at OFFIS. Other models were developed during a student project group at the University of Oldenburg. The models are intended to be used with the co-simulation framework mosaik (https://mosaik.offis.de), but are also usable without mosaik.

Version: 0.8

License: LGPL

## Installation

Pysimmods requires Python >= 3.6 and is available on https://pypi.org. 
It can be installed, preferably into a virtualenv,  with 

    >>> pip install pysimmods

Alternatively, you can clone this repository with

    >>> git clone https://gitlab.com/midas-mosaik/pysimmods.git

then switch into the pysimmods folder and type

    >>> pip install -e .

## Documentation

You will find a still growing documentation at https://midas-mosaik.gitlab.io/pysimmods.
Please create an issue if you find any errors or if you think that something should be explained more explicitly.
# Contributing

## If you want to contribute

* Create an issue
  + Describe what you want to add/improve/fix and why.
* If you want to provide the necessary code by your self
  + Mention in the issue that you want to do it
  + You will then get the permissions to create a merge request (directly from the issue)
  + Once the feature/improvement/fix is ready, commit+push your branch and *mark it as ready*. Note that you format the code with black (version 22.3.0) or the pipeline will fail.

## Contributors (in order of appearance):

Johannes Gerster <johannes.gerster@offis.de>
Stephan Balduin <stephan.balduin@offis.de>
Nils Wenninghoff <nils.wenninghoff@offis.de>
Paul Hendrik Tiemann
from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
from pysimmods.buffer.batterysim.battery import Battery
from pysimmods.buffer.batterysim.presets import battery_preset


def main():

    # First, create an instance of a battery model
    battery = Battery(*battery_preset(5))

    now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

    step_size = 900
    steps = 96
    index = np.arange(steps)
    power = np.zeros(steps)
    charge = np.zeros(steps)

    # Simulate for one day
    for i in range(steps):
        battery.set_step_size(step_size)
        battery.set_now_dt(now_dt)
        battery.step()

        power[i] = battery.get_p_kw()
        charge[i] = battery.state.soc_percent

        now_dt += timedelta(seconds=step_size)

    # Plot the results
    _, axes = plt.subplots(2, figsize=(6, 6))

    axes[0].bar(index, power, color="green")
    axes[0].set_ylabel("active power [kW]")

    axes[1].plot(charge, color="red")
    axes[1].set_xlabel("steps (15-minutes)")
    axes[1].set_ylabel("state of charge [%]")
    plt.savefig("battery_only.png", dpi=300, bbox_inches="tight")
    plt.close()

    # How long does it take to discharge the battery?
    index = []  # We don't know how long it takes; no numpy this time
    power = []
    charge = []

    battery.state.soc_percent = 100
    steps = 0

    while battery.state.soc_percent > battery.config.soc_min_percent:
        battery.set_step_size(step_size)
        battery.set_now_dt(now_dt)
        battery.set_p_kw(-1)
        battery.step()

        index.append(steps)
        power.append(battery.get_p_kw())
        charge.append(battery.state.soc_percent)

        steps += 1
        now_dt += timedelta(seconds=step_size)

    print(f"Number of steps: {steps}. Total duration: {steps*900/60} minutes.")
    # Plot the results
    _, axes = plt.subplots(2, figsize=(6, 6))

    axes[0].bar(index, power, color="green")
    axes[0].set_ylabel("active power [kW]")

    axes[1].plot(charge, color="red")
    axes[1].set_xlabel("steps (15-minutes)")
    axes[1].set_ylabel("state of charge [%]")
    plt.savefig("battery_discharge.png", dpi=300, bbox_inches="tight")
    plt.close()

    # How long does it take to charge the battery?
    index = []
    power = []
    charge = []

    steps = 0

    while battery.state.soc_percent < 100:
        battery.set_step_size(step_size)
        battery.set_now_dt(now_dt)
        battery.set_p_kw(1)
        battery.step()

        index.append(steps)
        power.append(battery.get_p_kw())
        charge.append(battery.state.soc_percent)

        steps += 1
        now_dt += timedelta(seconds=step_size)

    print(f"Number of steps: {steps}. Total duration: {steps*900/60} minutes.")
    # Plot the results
    _, axes = plt.subplots(2, figsize=(6, 6))

    axes[0].bar(index, power, color="green")
    axes[0].set_ylabel("active power [kW]")

    axes[1].plot(charge, color="red")
    axes[1].set_xlabel("steps (15-minutes)")
    axes[1].set_ylabel("state of charge [%]")
    plt.savefig("battery_charge.png", dpi=300, bbox_inches="tight")
    plt.close()


if __name__ == "__main__":
    main()

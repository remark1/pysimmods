from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
from pysimmods.generator.biogassim.biogas import BiogasPlant
from pysimmods.generator.biogassim.presets import biogas_preset


def main():
    pn_max_kw = 1500
    biogas = BiogasPlant(*biogas_preset(pn_max_kw))

    now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

    steps = 96
    step_size = 900
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    gas_fill = np.zeros(steps)
    gas_cons = np.zeros(steps)

    for i in range(steps):
        biogas.set_step_size(step_size)
        biogas.set_now_dt(now_dt)

        biogas.step()

        p_kws[i] = biogas.get_p_kw()
        p_th_kws[i] = biogas.state.p_th_kw
        gas_fill[i] = biogas.state.gas_fill_percent
        gas_cons[i] = biogas.state.gas_cons_m3

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, gas_fill, gas_cons, "biogas_only.png")

    steps *= 2
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    gas_fill = np.zeros(steps)
    gas_cons = np.zeros(steps)

    for i in range(steps):
        biogas.set_step_size(step_size)
        biogas.set_now_dt(now_dt)
        biogas.set_p_kw(pn_max_kw)

        biogas.step()

        p_kws[i] = biogas.get_p_kw()
        p_th_kws[i] = biogas.state.p_th_kw
        gas_fill[i] = biogas.state.gas_fill_percent
        gas_cons[i] = biogas.state.gas_cons_m3

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, gas_fill, gas_cons, "biogas_max_power.png")

    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    gas_fill = np.zeros(steps)
    gas_cons = np.zeros(steps)

    for i in range(steps):
        biogas.set_step_size(step_size)
        biogas.set_now_dt(now_dt)
        biogas.set_p_kw(np.random.randint(0, pn_max_kw))

        biogas.step()

        p_kws[i] = biogas.get_p_kw()
        p_th_kws[i] = biogas.state.p_th_kw
        gas_fill[i] = biogas.state.gas_fill_percent
        gas_cons[i] = biogas.state.gas_cons_m3

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, gas_fill, gas_cons, "biogas_random_power.png")


def plot(index, p_kws, p_th_kws, gas_fill, gas_cons, filename):
    # Plot the results
    _, axes = plt.subplots(2, 2, figsize=(12, 6))

    axes[0][0].bar(index, p_kws, color="green")
    axes[0][0].set_ylabel("active power [kW]")

    axes[0][1].bar(index, p_th_kws, color="orange")
    axes[0][1].set_ylabel("thermal power [kW]")
    axes[1][0].set_xlabel("steps (15-minutes)")

    axes[1][0].plot(gas_fill, color="red")
    axes[1][0].set_ylabel("gas fill [%]")

    axes[1][1].plot(gas_cons, color="black")
    axes[1][1].set_ylabel("gas consumption [m³]")

    axes[1][1].set_xlabel("steps (15-minutes)")
    plt.savefig(filename, dpi=300, bbox_inches="tight")
    plt.close()


if __name__ == "__main__":
    main()

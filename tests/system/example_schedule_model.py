from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pysimmods.buffer.batterysim.battery import Battery
from pysimmods.buffer.batterysim.presets import battery_preset
from pysimmods.other.flexibility.schedule_model import ScheduleModel


def main():

    # First, create an instance of a battery model
    battery = Battery(*battery_preset(5))

    # Second, pass the battery to the schedule model
    smodel = ScheduleModel(battery)
    hour = 0
    now_dt = datetime(2021, 6, 8, hour, 0, 0, tzinfo=timezone.utc)

    step_size = 900
    steps = 96
    index = np.arange(steps)
    power = np.zeros(steps)
    charge = np.zeros(steps)

    # Simulate for one day
    for i in range(steps):
        smodel.set_step_size(step_size)
        smodel.set_now_dt(now_dt)
        smodel.step()

        power[i] = smodel.get_p_kw()
        charge[i] = smodel.state.soc_percent

        now_dt += timedelta(seconds=step_size)

    # Get the default schedule, hourly shifted to match the timestamp
    ds = np.array(smodel.config.default_p_schedule)
    if hour > 0:
        default_schedule = np.zeros_like(ds)
        default_schedule[:-hour] = ds[hour:]
        default_schedule[-hour:] = ds[:hour]
    else:
        default_schedule = ds

    # Plot the results
    _, axes = plt.subplots(2, figsize=(6, 6))

    axes[0].bar(index, power, color="lightgreen")
    axes[0].plot(
        index,
        default_schedule.repeat(3600 // step_size),
        color="darkgreen",
    )
    axes[0].set_ylabel("active power [kW]")

    axes[1].plot(charge, color="red")
    axes[1].set_xlabel("steps (15-minutes)")
    axes[1].set_ylabel("state of charge [%]")
    plt.savefig("battery_only.png", dpi=300, bbox_inches="tight")
    plt.close()

    # Create a schedule for the battery
    schedule = pd.DataFrame(
        columns=["p_set_kw"],
        index=pd.date_range(
            now_dt + timedelta(hours=1),
            now_dt + timedelta(hours=24),
            freq=f"{step_size*3}S",
        ),
    )
    schedule.p_set_kw = 4

    # Pass the schedule to the battery
    smodel.update_schedule(schedule)

    power2 = np.zeros(steps)
    charge2 = np.zeros(steps)

    # Simulate another day
    for i in range(steps):
        smodel.set_step_size(step_size)
        smodel.set_now_dt(now_dt)
        smodel.step()

        power2[i] = smodel.get_p_kw()
        charge2[i] = smodel.state.soc_percent

        now_dt += timedelta(seconds=step_size)

    # Plot again
    _, axes = plt.subplots(2, figsize=(6, 6))

    axes[0].bar(
        index,
        [0, 0, 0] + ([0, 4, 0] * 31),
        edgecolor="orange",
        fc=(0, 0, 0, 0),
    )
    axes[0].bar(index, power2, color="lightgreen")
    axes[0].plot(
        index,
        default_schedule.repeat(3600 // step_size),
        color="darkgreen",
    )

    axes[0].set_ylabel("active power [kW]")

    axes[1].plot(charge2, color="red")
    axes[1].set_xlabel("steps (15-minutes)")
    axes[1].set_ylabel("state of charge [%]")
    plt.savefig("battery_schedule.png", dpi=300, bbox_inches="tight")
    plt.close()


if __name__ == "__main__":
    main()

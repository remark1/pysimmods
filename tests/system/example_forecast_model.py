import os
from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.generator.pvsystemsim.pvpsystem import PVPlantSystem

# from pysimmods.buffer.batterysim.battery import Battery
# from pysimmods.buffer.batterysim.presets import battery_presets
from pysimmods.other.flexibility.forecast_model import ForecastModel

T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
BH = "WeatherCurrent__0___bh_w_per_m2"
DH = "WeatherCurrent__0___dh_w_per_m2"
WD_PATH = os.path.abspath(
    os.path.join(__file__, "..", "..", "fixtures", "weather-time-series.csv")
)


def main():

    # First, load weather data that is required for the PV model
    # and prepare the forecast dataframe
    wd = pd.read_csv(WD_PATH, index_col=0)
    start_idx = 96 * 159
    end_idx = 96 * 160
    forecast = pd.DataFrame(
        data={
            "t_air_deg_celsius": wd.iloc[start_idx:end_idx][T_AIR].values,
            "bh_w_per_m2": wd.iloc[start_idx:end_idx][BH].values,
            "dh_w_per_m2": wd.iloc[start_idx:end_idx][DH].values,
        },
        index=pd.date_range(
            datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc),
            datetime(2021, 6, 8, 23, 45, 0, tzinfo=timezone.utc),
            freq="900S",
        ),
    )
    print(forecast)
    # Next, create an instance of a PV model
    pvsys = PVPlantSystem(*pv_preset(9))
    fcmodel = ForecastModel(pvsys, forecast_horizon_hours=2)
    fcmodel.update_forecasts(forecast)
    now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

    step_size = 900
    steps = 96
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    q_kvars = np.zeros(steps)
    p_forecast = np.zeros((steps, steps))
    q_forecast = np.zeros((steps, steps))

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        fcmodel.set_step_size(step_size)
        fcmodel.set_now_dt(now_dt)
        fcmodel.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
        fcmodel.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
        fcmodel.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

        fcmodel.step()

        p_kws[i] = fcmodel.get_p_kw()
        q_kvars[i] = fcmodel.get_q_kvar()

        if i + 9 < steps:
            for j in range(1, 9):
                curr_dt = now_dt + timedelta(seconds=step_size * j)
                p_forecast[i][i + j] = fcmodel.schedule.get(curr_dt, "p_kw")
                q_forecast[i][i + j] = fcmodel.schedule.get(curr_dt, "q_kvar")
        now_dt += timedelta(seconds=step_size)

    start = 40
    p_kws_before = np.zeros(steps)
    p_kws_after = np.zeros(steps)
    p_kws_before[:start] = p_kws[:start]
    p_kws_after[start:] = p_kws[start:]

    q_kvars_before = np.zeros(steps)
    q_kvars_after = np.zeros(steps)
    q_kvars_before[:start] = q_kvars[:start]
    q_kvars_after[start:] = q_kvars[start:]

    _, axes = plt.subplots(2, figsize=(6, 6))
    axes[0].bar(index, p_kws_before, color="green")
    axes[0].set_ylabel("active power [kW]")
    axes[1].bar(index, q_kvars_before, color="orange")
    axes[1].set_xlabel("steps (15-minutes)")
    axes[1].set_ylabel("reactive power [kVAr]")
    plt.savefig("pv_only.png", dpi=300, bbox_inches="tight")

    axes[0].bar(index, p_forecast[start - 1], edgecolor="red", fc=(0, 0, 0, 0))
    axes[1].bar(index, q_forecast[start - 1], edgecolor="red", fc=(0, 0, 0, 0))
    plt.savefig("pv_with_forecast.png", dpi=300, bbox_inches="tight")

    axes[0].bar(index, p_kws_after, color="lightgreen")
    axes[1].bar(index, q_kvars_after, color="yellow")
    axes[0].bar(index, p_forecast[start - 1], edgecolor="red", fc=(0, 0, 0, 0))
    axes[1].bar(index, q_forecast[start - 1], edgecolor="red", fc=(0, 0, 0, 0))
    plt.savefig("pv_with_actual.png", dpi=300, bbox_inches="tight")
    plt.close()


def datetime_to_index(
    dt: datetime, step_size: int = 900, steps_per_year: int = 35135
) -> int:
    dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
    dif = dif.total_seconds()
    idx = int(dif // step_size) % steps_per_year

    return idx


if __name__ == "__main__":
    main()

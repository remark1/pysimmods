import sys
import unittest

import numpy as np

try:
    from pysimmods.mosaik.flex_mosaik import FlexibilitySimulator
except ModuleNotFoundError:
    print("mosaik_api not found")
    pass

from pysimmods.mosaik.meta import META, MODELS
from pysimmods.other.dummy.model import DummyModel


class TestFlexMosaik(unittest.TestCase):
    def setUp(self):
        META["models"]["Dummy"] = {
            "public": True,
            "params": ["params", "inits"],
            "attrs": ["p_set_kw", "q_set_kvar"],
        }
        MODELS["Dummy"] = DummyModel

    @unittest.skipIf(
        "mosaik_api" not in sys.modules,
        "mosaik_api is not installed.",
    )
    def test_get_data(self):
        """Tests the usual mosaik API functions.

        init, create, step, and get_data will be tested.
        This tests a copy+paste from the pysim_mosaik test to check,
        if the usual functions work as expected.

        """
        sim = FlexibilitySimulator()

        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )
        inputs = {
            "Dummy-0": {
                "p_set_mw": {"DummySim-0.DummyMod-0": 68.92},
                "q_set_mvar": {"DummySim-0.DummyMod-0": 32.32},
            },
            "Dummy-1": {
                "set_percent": {
                    "DummySim-0.DummyMod-1": 50,
                }
            },
            "Dummy-2": {
                "p_set_kw": {"DummySim-0.DummyMod-0": 68920.0},
                "q_set_kvar": {"DummySim-0.DummyMod-0": 32320.0},
            },
        }
        sim.create(num=3, model="Dummy", params=dict(), inits=dict())

        sim.step(0, inputs)

        outputs = {
            "Dummy-0": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "Dummy-1": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "Dummy-2": ["p_mw", "p_kw", "q_mvar", "q_kvar", "schedule"],
        }

        data = sim.get_data(outputs)

        num_vals = 0
        self.assertIsInstance(data, dict)
        for eid, attrs in data.items():
            self.assertIsInstance(eid, str)
            self.assertIsInstance(attrs, dict)

            for attr, val in attrs.items():
                self.assertIsInstance(attr, str)
                if attr == "schedule":
                    self.assertIsInstance(val, str)
                else:
                    self.assertIsInstance(val, float)
                num_vals += 1

            self.assertEqual(attrs["p_mw"], attrs["p_kw"] * 1e-3)
            self.assertEqual(attrs["q_mvar"], attrs["q_kvar"] * 1e-3)

        self.assertEqual(num_vals, 13)

    @unittest.skipIf(
        "mosaik_api" not in sys.modules,
        "mosaik_api is not installed.",
    )
    def test_array_in_inputs(self):
        """This tests coveres if values are passed correctly when
        an input value is a numpy array instead of a numeric.
        """
        sim = FlexibilitySimulator()

        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )
        inputs = {
            "Dummy-0": {
                "p_set_mw": {"DummySim-0.DummyMod-0": 68.92},
            },
            "Dummy-1": {
                "set_percent": {
                    "DummySim-0.DummyMod-1": np.array([50], dtype=np.float32)
                }
            },
        }
        sim.create(num=2, model="Dummy", params=dict(), inits=dict())

        sim.step(0, inputs)

        outputs = {
            "Dummy-0": ["p_mw", "q_mvar"],
            "Dummy-1": ["p_mw", "q_mvar"],
        }

        data = sim.get_data(outputs)
        num_vals = 0
        self.assertIsInstance(data, dict)
        for eid, attrs in data.items():
            self.assertIsInstance(eid, str)
            self.assertIsInstance(attrs, dict)

            for attr, val in attrs.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 4)


if __name__ == "__main__":
    unittest.main()

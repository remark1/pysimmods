import unittest

from pysimmods.other.dummy.generator import DummyGenerator


class TestGenerator(unittest.TestCase):
    def test_set_p_kw_psc(self):
        """Test the correct behavior of the set_p_kw function."""

        gen = DummyGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        gen.set_p_kw(0)  # Turn off
        self.assertEqual(0, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(0, gen.get_p_kw())

        gen.set_p_kw(-500)  # Full generation
        self.assertEqual(500, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-500, gen.get_p_kw())

        gen.set_p_kw(100)  # Generator ignore the sign
        self.assertEqual(100, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-250, gen.get_p_kw())  # use pn min instead

        gen.set_p_kw(255)  # Consumption; invalid
        self.assertEqual(255, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-255, gen.get_p_kw())

        gen.set_p_kw(-250)  # Minimal generation
        self.assertEqual(250, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-250, gen.get_p_kw())

        gen.set_p_kw(-600)  # Generation too high
        self.assertEqual(600, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-500, gen.get_p_kw())

    def test_set_p_kw_asc(self):
        """Test the correct behavior of the set_p_kw function."""

        gen = DummyGenerator(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        gen.set_p_kw(0)  # Turn off
        self.assertEqual(0, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(0, gen.get_p_kw())

        gen.set_p_kw(500)  # Full generation
        self.assertEqual(500, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(500, gen.get_p_kw())

        gen.set_p_kw(-100)
        self.assertEqual(100, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(250, gen.get_p_kw())

        gen.set_p_kw(255)  # Consumption; invalid
        self.assertEqual(255, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(255, gen.get_p_kw())

        gen.set_p_kw(600)  # Generation too high
        self.assertEqual(600, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(500, gen.get_p_kw())

    @unittest.skip
    def test_get_percent_in_psc(self):
        gen = DummyGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        # Input not set
        self.assertIsNone(gen.get_percent_in())

        gen.set_p_kw(0)  # Turn off
        self.assertEqual(0, gen.get_percent_in())

        gen.set_p_kw(-500)  # Full generation
        self.assertEqual(100, gen.get_percent_in())

        gen.set_p_kw(-375)  # Half generation
        self.assertEqual(50, gen.get_percent_in())

        # Minimum generation, needs to be larger than zero
        gen.set_p_kw(-250)
        self.assertEqual(0.01, gen.get_percent_in())

        gen.set_p_kw(-125)  # Not feasible. Turn off
        self.assertEqual(0, gen.get_percent_in())

        gen.set_p_kw(125)  # Not feasible, can't consume energy
        self.assertEqual(0, gen.get_percent_in())

    @unittest.skip
    def test_get_percent_in_asc(self):
        """Same as above but signs are flipped."""
        gen = DummyGenerator(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        # Input not set
        self.assertIsNone(gen.get_percent_in())

        gen.set_p_kw(0)
        self.assertEqual(0, gen.get_percent_in())

        gen.set_p_kw(500)
        self.assertEqual(100, gen.get_percent_in())

        gen.set_p_kw(375)
        self.assertEqual(50, gen.get_percent_in())

        gen.set_p_kw(250)  # Needs to be larger than zero
        self.assertEqual(0.01, gen.get_percent_in())

        gen.set_p_kw(125)  # Not feasible. Turn off
        self.assertEqual(0, gen.get_percent_in())

        gen.set_p_kw(-125)  # Not feasible, can't consume energy
        self.assertEqual(0, gen.get_percent_in())

    @unittest.skip
    def test_set_percent_psc(self):
        gen = DummyGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        gen.set_percent(0)  # Turn off
        self.assertEqual(0, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(0, gen.get_p_kw())

        gen.set_percent(100)  # Full generation
        self.assertEqual(500, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-500, gen.get_p_kw())

        gen.set_percent(50)  # Half generation
        self.assertEqual(375, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-375, gen.get_p_kw())

        gen.set_percent(0.01)  # Minimum generation
        self.assertEqual(250, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-250, gen.get_p_kw())

        gen.set_percent(0.005)  # Will be mapped to minimum generation
        self.assertEqual(250, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-250, gen.get_p_kw())

        gen.set_percent(1)  # Very low generation
        self.assertEqual(252.5, gen.inputs.p_set_kw)
        gen.step()
        self.assertEqual(-252.5, gen.get_p_kw())

        gen.set_percent(-5)  # Invalid percent
        self.assertEqual(0, gen.inputs.p_set_kw)

        gen.set_percent(120)  # Invalid percent
        self.assertEqual(500, gen.inputs.p_set_kw)

    @unittest.skip
    def test_set_percent_asc(self):
        gen = DummyGenerator(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        gen.set_percent(0)  # Turn off
        self.assertEqual(0, gen.inputs.p_set_kw)

        gen.set_percent(100)  # Full generation
        self.assertEqual(500, gen.inputs.p_set_kw)

        gen.set_percent(50)  # Half generation
        self.assertEqual(375, gen.inputs.p_set_kw)

        gen.set_percent(0.01)  # Minimum generation
        self.assertEqual(250, gen.inputs.p_set_kw)

        gen.set_percent(0.005)  # Will be mapped to minimum generation
        self.assertEqual(250, gen.inputs.p_set_kw)

        gen.set_percent(1)  # Very low generation
        self.assertEqual(252.5, gen.inputs.p_set_kw)


if __name__ == "__main__":
    unittest.main()

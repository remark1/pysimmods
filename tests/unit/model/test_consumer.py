import unittest

from pysimmods.other.dummy.consumer import DummyConsumer


class TestConsumer(unittest.TestCase):
    def test_set_p_kw_psc(self):
        """Test the correct behavior of the set_p_kw function."""

        consumer = DummyConsumer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        consumer.set_p_kw(0)  # Turn off
        self.assertEqual(0, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(0, consumer.get_p_kw())

        consumer.set_p_kw(500)  # Full consumption
        self.assertEqual(500, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(500, consumer.get_p_kw())

        consumer.set_p_kw(-100)  # Sign will be ignored
        self.assertEqual(100, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(250, consumer.get_p_kw())

        consumer.set_p_kw(250)  # Minimal consumption
        self.assertEqual(250, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(250, consumer.get_p_kw())

        consumer.set_p_kw(100)  # Consumption too low
        self.assertEqual(100, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(250, consumer.get_p_kw())

        consumer.set_p_kw(600)  # Consumption too high
        self.assertEqual(600, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(500, consumer.get_p_kw())

    def test_set_p_kw_asc(self):
        """Test the correct behavior of the set_p_kw function."""

        consumer = DummyConsumer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        consumer.set_p_kw(0)  # Turn off
        self.assertEqual(0, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(0, consumer.get_p_kw())

        consumer.set_p_kw(-500)  # Full consumption
        self.assertEqual(500, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(-500, consumer.get_p_kw())

        consumer.set_p_kw(100)  # sign will be ignored
        self.assertEqual(100, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(-250, consumer.get_p_kw())

        consumer.set_p_kw(-250)  # Minimal consumption
        self.assertEqual(250, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(-250, consumer.get_p_kw())

        consumer.set_p_kw(-100)  # Consumption too low
        self.assertEqual(100, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(-250, consumer.get_p_kw())

        consumer.set_p_kw(-600)  # Consumption too high
        self.assertEqual(600, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(-500, consumer.get_p_kw())

    @unittest.skip
    def test_get_percent_in_psc(self):
        consumer = DummyConsumer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        # Input not set
        self.assertIsNone(consumer.get_percent_in())

        consumer.set_p_kw(0)  # Turn off
        self.assertEqual(0, consumer.get_percent_in())

        consumer.set_p_kw(500)  # Full consumption
        self.assertEqual(100, consumer.get_percent_in())

        consumer.set_p_kw(375)  # Half consumption
        self.assertEqual(50, consumer.get_percent_in())

        # Minimum consumption, needs to be larger than zero
        consumer.set_p_kw(250)
        self.assertEqual(0.01, consumer.get_percent_in())

        consumer.set_p_kw(125)  # Not feasible. Turn off
        self.assertEqual(0, consumer.get_percent_in())

        consumer.set_p_kw(-125)  # Not feasible, can't consume energy
        self.assertEqual(0, consumer.get_percent_in())

    @unittest.skip
    def test_get_percent_in_asc(self):
        """Same as above but signs are flipped."""
        consumer = DummyConsumer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        # Input not set
        self.assertIsNone(consumer.get_percent_in())

        consumer.set_p_kw(0)
        self.assertEqual(0, consumer.get_percent_in())

        consumer.set_p_kw(-500)
        self.assertEqual(100, consumer.get_percent_in())

        consumer.set_p_kw(-375)
        self.assertEqual(50, consumer.get_percent_in())

        consumer.set_p_kw(-250)  # Needs to be larger than zero
        self.assertEqual(0.01, consumer.get_percent_in())

        consumer.set_p_kw(-125)  # Not feasible. Turn off
        self.assertEqual(0, consumer.get_percent_in())

        consumer.set_p_kw(125)  # Not feasible, can't consume energy
        self.assertEqual(0, consumer.get_percent_in())

    @unittest.skip
    def test_set_percent_psc(self):
        consumer = DummyConsumer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        consumer.set_percent(0)  # Turn off
        self.assertEqual(0, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(0, consumer.get_p_kw())

        consumer.set_percent(100)  # Full consumption
        self.assertEqual(500, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(500, consumer.get_p_kw())

        consumer.set_percent(50)  # Half consumption
        self.assertEqual(375, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(375, consumer.get_p_kw())

        consumer.set_percent(0.01)  # Minimum consumption
        self.assertEqual(250, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(250, consumer.get_p_kw())

        consumer.set_percent(0.005)  # Will be mapped to minimum consumption
        self.assertEqual(250, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(250, consumer.get_p_kw())

        consumer.set_percent(1)  # Very low consumption
        self.assertEqual(252.5, consumer.inputs.p_set_kw)
        consumer.step()
        self.assertEqual(252.5, consumer.get_p_kw())

        consumer.set_percent(-5)  # Invalid percent
        self.assertEqual(0, consumer.inputs.p_set_kw)

        consumer.set_percent(120)  # Invalid percent
        self.assertEqual(500, consumer.inputs.p_set_kw)

    @unittest.skip
    def test_set_percent_asc(self):
        consumer = DummyConsumer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        consumer.set_percent(0)  # Turn off
        self.assertEqual(0, consumer.inputs.p_set_kw)

        consumer.set_percent(100)  # Full consumption
        self.assertEqual(500, consumer.inputs.p_set_kw)

        consumer.set_percent(50)  # Half consumption
        self.assertEqual(375, consumer.inputs.p_set_kw)

        consumer.set_percent(0.01)  # Minimum consumption
        self.assertEqual(250, consumer.inputs.p_set_kw)

        consumer.set_percent(0.005)  # Will be mapped to minimum consumption
        self.assertEqual(250, consumer.inputs.p_set_kw)

        consumer.set_percent(1)  # Very low consumption
        self.assertEqual(252.5, consumer.inputs.p_set_kw)


if __name__ == "__main__":
    unittest.main()

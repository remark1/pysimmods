import unittest

from pysimmods.other.dummy.buffer import DummyBuffer


class TestBuffer(unittest.TestCase):
    def test_set_p_kw(self):
        """Test the correct behavior of the set_p_kw function."""

        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        buffer.set_p_kw(0)  # Turn off
        self.assertEqual(0, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(0, buffer.get_p_kw())

        buffer.set_p_kw(500)  # Full charging/consumption
        self.assertEqual(500, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(500, buffer.get_p_kw())

        buffer.set_p_kw(-375)  # Half discharging/generation
        self.assertEqual(-375, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(-375, buffer.get_p_kw())

        buffer.set_p_kw(250)  # Minimal charging
        self.assertEqual(250, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(250, buffer.get_p_kw())

        buffer.set_p_kw(-100)  # Discharging too low
        self.assertEqual(0, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(0, buffer.get_p_kw())

        buffer.set_p_kw(100)  # Charging too low
        self.assertEqual(0, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(0, buffer.get_p_kw())

        buffer.set_p_kw(-600)  # Discharging too high
        self.assertEqual(-500, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(-500, buffer.get_p_kw())

        buffer.set_p_kw(600)  # Charging too high
        self.assertEqual(500, buffer.inputs.p_set_kw)
        buffer.step()
        self.assertEqual(500, buffer.get_p_kw())

    def test_get_pn_max_kw(self):
        """Test if get_pn_max_kw gives the correct results with both
        sign conventions. With passive sign convention, pn_max_kw is the
        maximum charging power.
        """
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        self.assertEqual(500, buffer.get_pn_max_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        self.assertEqual(500, buffer.get_pn_max_kw())

    def test_get_pn_min_kw(self):
        """Test if get_pn_min_kw gives the correct results with both
        sign conventions. With passive sign convention, pn_min_kw is the
        maximum discharging power.
        """
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        self.assertEqual(-500, buffer.get_pn_min_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )

        self.assertEqual(-500, buffer.get_pn_min_kw())

    def test_get_pn_charge_max_kw(self):
        """Test the correct output of get_pn_charge_max_kw.

        If the attribute is present, it should be returned with positive
        sign in passive sign convention. Otherwise, p_max_kw will be
        returned.
        """
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(500, buffer.get_pn_charge_max_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_charge_max_kw": 450,
            },
            dict(),
        )
        self.assertEqual(450, buffer.get_pn_charge_max_kw())

        # Active sign convention
        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(-500, buffer.get_pn_charge_max_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_charge_max_kw": 450,
            },
            dict(),
        )
        self.assertEqual(-450, buffer.get_pn_charge_max_kw())

    def test_get_pn_charge_min_kw(self):
        """Test the correct output of get_pn_charge_min_kw.

        If the attribute is present, it should be returned with positive
        sign in passive sign convention. Otherwise, p_min_kw will be
        returned.

        """
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(250, buffer.get_pn_charge_min_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_charge_min_kw": 300,
            },
            dict(),
        )
        self.assertEqual(300, buffer.get_pn_charge_min_kw())

        # Active sign convention
        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(-250, buffer.get_pn_charge_min_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_charge_min_kw": 300,
            },
            dict(),
        )
        self.assertEqual(-300, buffer.get_pn_charge_min_kw())

    def test_get_pn_discharge_max_kw(self):
        """Test the correct output of get_pn_discharge_max_kw.

        If the attribute is present, it should be returned with negative
        sign in passive sign convention. Otherwise, p_max_kw will be
        returned with negative sign.
        """
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(-500, buffer.get_pn_discharge_max_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_discharge_max_kw": 450,
            },
            dict(),
        )
        self.assertEqual(-450, buffer.get_pn_discharge_max_kw())

        # Active sign convention
        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(500, buffer.get_pn_discharge_max_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_discharge_max_kw": 450,
            },
            dict(),
        )
        self.assertEqual(450, buffer.get_pn_discharge_max_kw())

    def test_get_pn_discharge_min_kw(self):
        """Test the correct output of get_pn_discharge_min_kw.

        If the attribute is present, it should be returned with negative
        sign in passive sign convention. Otherwise, p_min_kw will be
        returned with negative sign.

        """
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(-250, buffer.get_pn_discharge_min_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_discharge_min_kw": 300,
            },
            dict(),
        )
        self.assertEqual(-300, buffer.get_pn_discharge_min_kw())

        # Active sign convention
        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        self.assertEqual(250, buffer.get_pn_discharge_min_kw())

        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "p_discharge_min_kw": 300,
            },
            dict(),
        )
        self.assertEqual(300, buffer.get_pn_discharge_min_kw())

    @unittest.skip
    def test_get_percent_in_psc(self):
        """Test if the get_percent_in method returns the correct values."""
        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        buffer.set_p_kw(0)  # No operation
        self.assertEqual(50, buffer.get_percent_in())

        buffer.set_p_kw(500)  # Full charging
        self.assertEqual(100, buffer.get_percent_in())

        buffer.set_p_kw(-375)  # Half discharging
        self.assertEqual(25, buffer.get_percent_in())

        buffer.set_p_kw(-250)  # Minimal discharging (> 0)
        self.assertEqual(49.995, buffer.get_percent_in())

        buffer.set_p_kw(250)  # Minimal charging (> 0)
        self.assertEqual(50.005, buffer.get_percent_in())

        buffer.set_p_kw(-125)  # Not feasible, too low. Turn off
        self.assertEqual(50, buffer.get_percent_in())

    @unittest.skip
    def test_get_percent_in_asc(self):
        """Test buffer with active sign convention.

        The results are the same as with passive sign convention!
        This is because we flip semantics of charge and discharge but
        this does not change the percentage. However, the semantic of
        percentage changed: 100 % now means full discharging instead of
        charging.

        """
        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        buffer.set_p_kw(0)  # No operation
        self.assertEqual(50, buffer.get_percent_in())

        buffer.set_p_kw(500)  # Full discharging
        self.assertEqual(100, buffer.get_percent_in())

        buffer.set_p_kw(-375)  # Half charging
        self.assertEqual(25, buffer.get_percent_in())

        buffer.set_p_kw(-250)  # Minimal charging (> 0)
        self.assertEqual(49.995, buffer.get_percent_in())

        buffer.set_p_kw(250)  # Minimal discharging (> 0)
        self.assertEqual(50.005, buffer.get_percent_in())

        buffer.set_p_kw(-125)  # Not feasible, too low. Turn off
        self.assertEqual(50, buffer.get_percent_in())

    @unittest.skip
    def test_set_percent_psc(self):
        """Test buffer with passive sign convention."""

        buffer = DummyBuffer(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        buffer.set_percent(50)  # Turn off
        self.assertEqual(0, buffer.inputs.p_set_kw)

        buffer.set_percent(100)  # Full charging
        self.assertEqual(500, buffer.inputs.p_set_kw)

        buffer.set_percent(25)  # Half discharging
        self.assertEqual(-375, buffer.inputs.p_set_kw)

        buffer.set_percent(49.995)  # Minimum discharging, rounding errors
        self.assertAlmostEqual(-250.025, buffer.inputs.p_set_kw)

        # Value too close to 50. Will be mapped to 49.995.
        buffer.set_percent(49.9950001)
        self.assertAlmostEqual(-250.025, buffer.inputs.p_set_kw)

        buffer.set_percent(50.005)  # Minimum charging, rounding errors
        self.assertAlmostEqual(250.025, buffer.inputs.p_set_kw)

    @unittest.skip
    def test_set_percent_asc(self):
        """Test buffer with active sign convention.

        The results are the same as with passive sign convention!

        """
        buffer = DummyBuffer(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
            },
            dict(),
        )
        buffer.set_percent(50)  # Turn off
        self.assertEqual(0, buffer.inputs.p_set_kw)

        buffer.set_percent(100)  # Full discharging
        self.assertEqual(500, buffer.inputs.p_set_kw)

        buffer.set_percent(25)  # Half charging
        self.assertEqual(-375, buffer.inputs.p_set_kw)

        buffer.set_percent(49.995)  # Minimum charging, rounding errors
        self.assertAlmostEqual(-250.025, buffer.inputs.p_set_kw)

        buffer.set_percent(50.005)  # Minimum discharging, rounding errors
        self.assertAlmostEqual(250.025, buffer.inputs.p_set_kw)


if __name__ == "__main__":
    unittest.main()

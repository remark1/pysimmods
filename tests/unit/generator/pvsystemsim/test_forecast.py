"""This module contains the tests for the :class:`.ForecastModel`."""
import unittest
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.generator.pvsystemsim.pvpsystem import PVPlantSystem
from pysimmods.other.flexibility.forecast_model import ForecastModel
from pysimmods.util.date_util import GER


class TestForecastModel(unittest.TestCase):
    def setUp(self):

        self.start_date = "2006-08-01 12:00:00+0100"

        bhs = (
            [505, 510, 520, 530, 540, 550, 560, 570]
            + [580, 590, 600, 610, 600, 590, 580, 570]
            + [560, 550, 540, 530, 520, 510, 500]
            + [490, 500, 510, 520, 530, 540, 550]
        )
        dhs = (
            [305, 310, 320, 330, 340, 350, 360]
            + [370, 380, 390, 400, 410, 410, 400, 390]
            + [380, 370, 360, 350, 340, 330, 320]
            + [310, 300, 300, 310, 320, 330, 340, 350]
        )
        t_airs = (
            [20.2, 20.5, 21, 21.5, 22, 22.5, 23, 23, 23]
            + [23, 23, 23, 22.5, 22.5, 22.5, 22.5]
            + [22.5, 22.5, 22, 21.5, 21, 20.5, 20]
            + [19.5, 19, 19, 19, 19, 19, 19]
        )
        index = pd.date_range(
            datetime.strptime(self.start_date, GER),
            datetime.strptime(self.start_date, GER)
            + timedelta(hours=2, minutes=25),
            periods=30,
        )
        self._forecasts = pd.DataFrame(
            data=np.array([bhs, dhs, t_airs]).transpose(),
            columns=[
                "bh_w_per_m2",
                "dh_w_per_m2",
                "t_air_deg_celsius",
            ],
            index=index,
        )

        self._forecasts_p1 = pd.DataFrame(
            data=np.array([bhs[:4], dhs[:4], t_airs[:4]]).transpose(),
            columns=[
                "bh_w_per_m2",
                "dh_w_per_m2",
                "t_air_deg_celsius",
            ],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=15),
                periods=4,
            ),
        )

        self._forecasts_p2 = pd.DataFrame(
            data=np.array([bhs[2:6], dhs[2:6], t_airs[2:6]]).transpose(),
            columns=[
                "bh_w_per_m2",
                "dh_w_per_m2",
                "t_air_deg_celsius",
            ],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER) + timedelta(minutes=5),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=20),
                periods=4,
            ),
        )

        self._forecasts_p3 = pd.DataFrame(
            data=np.array([bhs[4:8], dhs[4:8], t_airs[4:8]]).transpose(),
            columns=[
                "bh_w_per_m2",
                "dh_w_per_m2",
                "t_air_deg_celsius",
            ],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=10),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=25),
                periods=4,
            ),
        )

        self._forecasts_bh_w_1 = pd.DataFrame(
            data=np.array([bhs[:4]]).transpose(),
            columns=["bh_w_per_m2"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=15),
                periods=4,
            ),
        )

        self._forecasts_bh_w_2 = pd.DataFrame(
            data=np.array([bhs[2:6]]).transpose(),
            columns=["bh_w_per_m2"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER) + timedelta(minutes=5),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=20),
                periods=4,
            ),
        )

        self._forecasts_dh_w_1 = pd.DataFrame(
            data=np.array([dhs[:4]]).transpose(),
            columns=["dh_w_per_m2"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=15),
                periods=4,
            ),
        )

        self._forecasts_dh_w_2 = pd.DataFrame(
            data=np.array([dhs[2:6]]).transpose(),
            columns=["dh_w_per_m2"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER) + timedelta(minutes=5),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=20),
                periods=4,
            ),
        )

        self._forecasts_t_air_1 = pd.DataFrame(
            data=np.array([t_airs[:4]]).transpose(),
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=15),
                periods=4,
            ),
        )

        self._forecasts_t_air_2 = pd.DataFrame(
            data=np.array([t_airs[2:6]]).transpose(),
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER) + timedelta(minutes=5),
                datetime.strptime(self.start_date, GER)
                + timedelta(minutes=20),
                periods=4,
            ),
        )

        self.schedule = pd.DataFrame(
            data=np.ones(30), columns=["target"], index=index
        )

    def test_step(self):
        """Test the regular step of the underlying model.

        Using only the step method, the results should be the same
        results. Additionally, the forecast model keeps track of the
        current time.

        """
        pvp = PVPlantSystem(*pv_preset(2.25))
        fcm = ForecastModel(PVPlantSystem(*pv_preset(2.25)))

        pvp.set_now_dt(self.start_date)
        pvp.set_step_size(5 * 60)
        pvp.set_p_kw(1.2)

        pvp.inputs.bh_w_per_m2 = 500
        pvp.inputs.dh_w_per_m2 = 300
        pvp.inputs.t_air_deg_celsius = 20
        pvp.step()

        self.assertAlmostEqual(1.2, pvp.get_p_kw())
        self.assertAlmostEqual(-0.58118652, pvp.get_q_kvar())
        self.assertAlmostEqual(23.4442154, pvp.state.t_module_deg_celsius)

        fcm.set_now_dt(self.start_date)
        fcm.set_step_size(5 * 60)
        fcm.set_p_kw(1.2)

        fcm.inputs.bh_w_per_m2 = 500
        fcm.inputs.dh_w_per_m2 = 300
        fcm.inputs.t_air_deg_celsius = 20

        fcm.step()

        self.assertEqual(fcm.get_p_kw(), pvp.get_p_kw())
        self.assertEqual(fcm.get_q_kvar(), pvp.get_q_kvar())
        self.assertEqual(
            fcm.state.t_module_deg_celsius,
            pvp.state.t_module_deg_celsius,
        )

    def test_update_forecasts(self):
        """Test if forecasts are updated correctly."""
        flex = ForecastModel(PVPlantSystem(*pv_preset(9)))
        flex.update_forecasts(self._forecasts_p1)

        # print(flex._forecasts)
        self.assertIsInstance(flex._forecasts, pd.DataFrame)
        self.assertEqual(flex._forecasts.size, 12)
        outdated_val = flex._forecasts["bh_w_per_m2"].values[1]
        self.assertEqual(outdated_val, 510)
        flex.update_forecasts(self._forecasts_p2)

        # print(flex._forecasts)
        self.assertIsInstance(flex._forecasts, pd.DataFrame)
        self.assertEqual(flex._forecasts.size, 15)
        updated_val = flex._forecasts["bh_w_per_m2"].values[1]
        self.assertNotEqual(outdated_val, updated_val)

        flex.update_forecasts(self._forecasts_p3)

        # print(flex._forecasts)
        self.assertIsInstance(flex._forecasts, pd.DataFrame)
        self.assertEqual(flex._forecasts.size, 18)

    def test_update_forecasts_one_at_a_time(self):
        flex = ForecastModel(PVPlantSystem(*pv_preset(9)))
        self.assertEqual(flex._forecasts, None)

        flex.update_forecasts(self._forecasts_bh_w_1)
        # print(flex._forecasts)
        self.assertEqual(flex._forecasts.size, 4)
        self.assertIn("bh_w_per_m2", flex._forecasts.columns)
        self.assertNotIn("dh_w_per_m2", flex._forecasts.columns)
        self.assertNotIn("t_air_deg_celsius", flex._forecasts.columns)

        flex.update_forecasts(self._forecasts_dh_w_1)
        # print(flex._forecasts)
        self.assertEqual(flex._forecasts.size, 8)
        self.assertIn("bh_w_per_m2", flex._forecasts.columns)
        self.assertIn("dh_w_per_m2", flex._forecasts.columns)
        self.assertNotIn("t_air_deg_celsius", flex._forecasts.columns)

        flex.update_forecasts(self._forecasts_t_air_1)
        # print(flex._forecasts)
        self.assertEqual(flex._forecasts.size, 12)
        self.assertIn("bh_w_per_m2", flex._forecasts.columns)
        self.assertIn("dh_w_per_m2", flex._forecasts.columns)
        self.assertIn("t_air_deg_celsius", flex._forecasts.columns)

        # Second round
        flex.update_forecasts(self._forecasts_bh_w_2)
        # print(flex._forecasts)
        self.assertEqual(flex._forecasts.size, 15)
        self.assertIn("bh_w_per_m2", flex._forecasts.columns)
        self.assertIn("dh_w_per_m2", flex._forecasts.columns)
        self.assertIn("t_air_deg_celsius", flex._forecasts.columns)

        flex.update_forecasts(self._forecasts_dh_w_2)
        # print(flex._forecasts)
        self.assertEqual(flex._forecasts.size, 15)
        self.assertIn("bh_w_per_m2", flex._forecasts.columns)
        self.assertIn("dh_w_per_m2", flex._forecasts.columns)
        self.assertIn("t_air_deg_celsius", flex._forecasts.columns)

        flex.update_forecasts(self._forecasts_t_air_2)
        # print(flex._forecasts)
        self.assertEqual(flex._forecasts.size, 15)
        self.assertIn("bh_w_per_m2", flex._forecasts.columns)
        self.assertIn("dh_w_per_m2", flex._forecasts.columns)
        self.assertIn("t_air_deg_celsius", flex._forecasts.columns)


if __name__ == "__main__":
    unittest.main()

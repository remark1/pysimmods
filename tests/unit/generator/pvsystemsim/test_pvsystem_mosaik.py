import sys
import unittest

try:
    from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator
except ModuleNotFoundError:
    print("mosaik_api not found")
    pass

from pysimmods.generator.pvsystemsim.presets import pv_preset


class TestPysimMosaik(unittest.TestCase):
    @unittest.skipIf(
        "mosaik_api" not in sys.modules,
        "mosaik_api is not installed.",
    )
    def test_get_data(self):
        """Test the usual functions of the mosaik simulator."""
        sim = PysimmodsSimulator()

        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )
        inputs = {
            "Photovoltaic-0": {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
                "p_set_mw": {"DummySim-0.DummyMod-0": 0.6892},
            },
            "Photovoltaic-1": {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
            },
            "Photovoltaic-2": {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
                "p_set_kw": {"DummySim-0.DummyMod-0": 68920.0},
            },
        }
        params, inits = pv_preset(9)
        sim.create(num=3, model="Photovoltaic", params=params, inits=inits)

        sim.step(0, inputs)

        outputs = {
            "Photovoltaic-0": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "Photovoltaic-1": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "Photovoltaic-2": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
        }

        data = sim.get_data(outputs)

        num_vals = 0
        self.assertIsInstance(data, dict)
        for eid, attrs in data.items():
            self.assertIsInstance(eid, str)
            self.assertIsInstance(attrs, dict)

            for attr, val in attrs.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1
            self.assertEqual(attrs["p_mw"], attrs["p_kw"] * 1e-3)
            self.assertEqual(attrs["q_mvar"], attrs["q_kvar"] * 1e-3)
        self.assertEqual(num_vals, 12)

    @unittest.skip
    def test_flip_inverter_mode(self):
        sim = PysimmodsSimulator()

        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )
        eid1 = "Photovoltaic-0"
        eid2 = "Photovoltaic-1"

        inputs = {
            eid1: {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
                "inverter_inductive": {"DummySim-0.DummyMod-0": 0},
            },
            eid2: {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
            },
        }
        params, inits = pv_preset(9)
        sim.create(num=2, model="Photovoltaic", params=params, inits=inits)

        sim.step(0, inputs)
        outputs = {
            eid1: ["p_mw", "q_mvar", "inverter_inductive"],
            eid2: ["p_mw", "q_mvar", "inverter_inductive"],
        }

        data = sim.get_data(outputs)
        self.assertEqual(0, data[eid1]["inverter_inductive"])
        self.assertEqual(1, data[eid2]["inverter_inductive"])
        self.assertEqual(data[eid1]["q_mvar"], -data[eid2]["q_mvar"])

    @unittest.skip
    def test_flip_inverter_mode_and_set_percent(self):
        sim = PysimmodsSimulator()

        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )
        eid1 = "Photovoltaic-0"
        eid2 = "Photovoltaic-1"

        params, inits = pv_preset(9000, q_control="qp_set")
        sim.create(num=2, model="Photovoltaic", params=params, inits=inits)

        inputs = {
            eid1: {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
                "set_percent": {"DummySim-0.DummyMod-0": 75},
                "inverter_inductive": {"DummySim-0.DummyMod-0": 0},
            },
            eid2: {
                "bh_w_per_m2": {"DummySim-0.DummyMod-0": 500},
                "dh_w_per_m2": {"DummySim-0.DummyMod-0": 300},
                "set_percent": {"DummySim-0.DummyMod-0": 75},
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 20},
            },
        }
        sim.step(0, inputs)

        outputs = {
            eid1: ["p_mw", "q_mvar", "inverter_inductive"],
            eid2: ["p_mw", "q_mvar", "inverter_inductive"],
        }
        data = sim.get_data(outputs)

        self.assertEqual(0, data[eid1]["inverter_inductive"])
        self.assertEqual(1, data[eid2]["inverter_inductive"])
        self.assertEqual(data[eid1]["q_mvar"], -data[eid2]["q_mvar"])


if __name__ == "__main__":
    unittest.main()

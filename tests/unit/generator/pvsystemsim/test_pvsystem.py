import unittest

from pysimmods.generator.pvsim import PhotovoltaicPowerPlant
from pysimmods.generator.pvsystemsim import PVPlantSystem
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.other.invertersim import Inverter


class TestPVSystem(unittest.TestCase):
    def test_init(self):
        pv_system = PVPlantSystem(*pv_preset(2.25))

        self.assertIsInstance(pv_system.pv, PhotovoltaicPowerPlant)
        self.assertIsInstance(pv_system.inverter, Inverter)

    def test_step_p_set(self):
        """Test a step in the default mode 'p_set'."""
        pv_system = PVPlantSystem(*pv_preset(2.25))

        pv_system.set_step_size(5 * 60)
        pv_system.set_now_dt("2006-08-01 12:00:00+0000")
        pv_system.inputs.bh_w_per_m2 = 500
        pv_system.inputs.dh_w_per_m2 = 300
        pv_system.inputs.t_air_deg_celsius = 20
        pv_system.set_p_kw(-1.2)
        pv_system.step()

        self.assertEqual(1.2, pv_system.get_p_kw())
        self.assertAlmostEqual(-0.58118652, pv_system.get_q_kvar())
        self.assertEqual(0.9, pv_system.get_cos_phi())

    def test_step_q_set(self):
        """Test a step in the q_control mode."""
        pv_system = PVPlantSystem(*pv_preset(2.25))

        pv_system.set_step_size(5 * 60)
        pv_system.set_now_dt("2006-08-01 12:00:00+0000")
        pv_system.inputs.bh_w_per_m2 = 500
        pv_system.inputs.dh_w_per_m2 = 300
        pv_system.inputs.t_air_deg_celsius = 20

        pv_system.set_q_kvar(3)

        pv_system.step()
        self.assertAlmostEqual(2.1323788, pv_system.get_p_kw())
        self.assertAlmostEqual(1.3049754, pv_system.get_q_kvar())
        self.assertAlmostEqual(0.6489512, pv_system.get_cos_phi())


if __name__ == "__main__":
    unittest.main()

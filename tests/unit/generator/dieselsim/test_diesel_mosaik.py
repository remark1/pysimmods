import sys
import unittest

try:
    from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator
except ModuleNotFoundError:
    print("mosaik_api not found")
    pass

from pysimmods.generator.dieselsim.presets import diesel_presets


class TestDieselGeneratorMosaik(unittest.TestCase):
    @unittest.skipIf(
        "mosaik_api" not in sys.modules,
        "mosaik_api is not installed.",
    )
    def test_get_data(self):
        sim = PysimmodsSimulator()
        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2021-02-22 08:00:00+0100",
        )
        inputs = {
            "DieselGenerator-0": {"p_set_kw": {"DummySim-0.DummyMod-0": 10}},
            "DieselGenerator-1": {"set_percent": {"DummySim-0.DummyMod-1": 0}},
        }

        params, inits = diesel_presets(10)
        sim.create(num=2, model="DieselGenerator", params=params, inits=inits)

        sim.step(0, inputs)

        outputs = {
            "DieselGenerator-0": ["p_mw", "q_mvar"],
            "DieselGenerator-1": ["p_mw", "q_mvar"],
        }

        data = sim.get_data(outputs)
        num_vals = 0
        self.assertIsInstance(data, dict)
        for key, value in data.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, dict)

            for attr, val in value.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 4)


if __name__ == "__main__":
    unittest.main()

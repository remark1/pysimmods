"""Tests for the CHP model."""
import unittest

from pysimmods.generator.chplpgsim import CHPLPG


class TestCHPLPG(unittest.TestCase):
    """Testcase for the CHP."""

    def setUp(self):
        self.params = {
            "p_max_kw": 7.0,
            "p_min_kw": 3.5,
            "p_2_p_th_percent": 257.0,
            "eta_max_percent": 86.1,
            "eta_min_percent": 80.5,
            "own_consumption_kw": 0.55,
            "active_min_s": 0.0,
            "inactive_min_s": 0.0,
            "lubricant_max_l": 10.0,
            "lubricant_ml_per_h": 10.0,
            "storage_cap_l": 1050.0,
            "storage_consumption_kwh_per_day": 0.75,
            "storage_t_min_c": 40.0,
            "storage_t_max_c": 85.0,
        }

        self.inits = {
            "lubricant_l": 9,
            "active_s": 1800,
            "inactive_s": 0,
            "is_active": True,
            "storage_t_c": 75,
        }

    def test_step(self):
        """Check the normal step of the CHP."""
        chp = CHPLPG(self.params, self.inits)

        chp.set_p_kw(-7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5

        chp.step()

        self.assertEqual(chp.get_p_kw(), -7)
        self.assertEqual(chp.state.p_th_kw, 17.99)

        chp = CHPLPG(self.params, self.inits)
        chp.set_p_kw(7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5

        chp.step()

        self.assertEqual(chp.get_p_kw(), -7)
        self.assertEqual(chp.state.p_th_kw, 17.99)

    def test_default_schedule(self):
        """Check the operation if no setpoint is provided."""
        chp = CHPLPG(self.params, self.inits)

        chp.set_now_dt("2017-01-01 00:00:00+0000")
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5

        chp.step()

        self.assertEqual(chp.get_p_kw(), -3.5)
        self.assertEqual(chp.state.p_th_kw, 8.995)

    @unittest.skip
    def test_set_percent(self):
        """Check the functionality of the set_percent method."""
        chp = CHPLPG(self.params, self.inits)

        chp.set_percent(100)

        self.assertEqual(chp.inputs.p_set_kw, 7)

        chp.set_percent(0)
        self.assertEqual(chp.inputs.p_set_kw, 0)

        # No values between pn_min_kw and 0
        chp.set_percent(1)
        self.assertEqual(chp.inputs.p_set_kw, 3.535)

    def test_sign_convention(self):
        """Check if the output is correct according to the sign
        convention.
        """
        self.params["sign_convention"] = "passive"
        chp = CHPLPG(self.params, self.inits)
        state = chp.get_state()
        chp.set_p_kw(7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5
        chp.step()
        self.assertEqual(chp.get_p_kw(), -7)

        chp.set_state(state)
        chp.set_p_kw(-7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5
        chp.step()
        self.assertEqual(chp.get_p_kw(), -7)

        chp.set_state(state)
        chp.set_p_kw(7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5
        chp.step()
        self.assertEqual(chp.get_p_kw(), -7)

        self.params["sign_convention"] = "active"
        chp = CHPLPG(self.params, self.inits)
        state = chp.get_state()

        chp.set_state(state)
        chp.set_p_kw(7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5
        chp.step()
        self.assertEqual(chp.get_p_kw(), 7)

        chp.set_state(state)
        chp.set_p_kw(-7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5
        chp.step()
        self.assertEqual(chp.get_p_kw(), 7)

        chp.set_state(state)
        chp.set_p_kw(7)
        chp.set_step_size(900)
        chp.inputs.e_th_demand_set_kwh = 5
        chp.step()

        self.assertEqual(chp.get_p_kw(), 7)


if __name__ == "__main__":
    unittest.main()

"""Tests for the CHP CNG model.

TODO: Test the edge cases.

"""
import unittest

from pysimmods.generator.chpcngsim import CHPCNG


class TestCHPCNG(unittest.TestCase):
    """Testcase for the CHP CNG."""

    def setUp(self):
        self.params = {
            "pn_stages_kw": [680, 1130],
            "eta_stages_percent": [37.2, 43.9],
            "eta_th_stages_percent": [40.0, 43.9],
            "restarts_per_day": 2,
            "active_min_s": 0,
            "active_max_s_per_day": 14 * 3_600,
            "inactive_min_s": 2 * 3_600,
            "inactive_max_s_per_day": 0,
        }

        self.inits = {
            "active_s": 0,
            "active_s_per_day": 0,
            "inactive_s": 0,
            "inactive_s_per_day": 0,
            "restarts": 0,
            "p_kw": 680,
        }

    def test_step(self):
        """Test the normal step of the CHP."""
        chp = CHPCNG(self.params, self.inits)

        chp.set_p_kw(-680)
        chp.set_step_size(900)
        chp.inputs.gas_in_m3 = 95
        chp.set_now_dt("2017-01-01 00:00:00+0100")

        chp.step()

        self.assertEqual(chp.state.p_kw, 680)
        self.assertEqual(chp.get_p_kw(), -680)
        self.assertAlmostEqual(chp.state.p_th_kw, 731.1827957)
        self.assertAlmostEqual(chp.state.gas_cons_m3, 91.3975058)
        self.assertEqual(chp.state.active_s, 900)
        self.assertEqual(chp.state.active_s_per_day, 900)
        self.assertEqual(chp.state.inactive_s, 0)
        self.assertEqual(chp.state.inactive_s_per_day, 0)
        self.assertEqual(chp.state.restarts, 0)

    def test_default_schedule(self):
        chp = CHPCNG(self.params, self.inits)

        chp.set_step_size(900)
        chp.inputs.gas_in_m3 = 125
        chp.set_now_dt("2017-01-01 00:00:00+0000")

        chp.step()

        self.assertEqual(-905, chp.get_p_kw())


if __name__ == "__main__":
    unittest.main()

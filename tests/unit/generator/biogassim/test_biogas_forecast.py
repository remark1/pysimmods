"""This module contains tests for the biogas plant in combination
with the flexibility model.
"""
import unittest
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from pysimmods.generator.biogassim import BiogasPlant
from pysimmods.generator.biogassim.presets import biogas_preset
from pysimmods.other.flexibility.forecast_model import ForecastModel
from pysimmods.util.date_util import GER


class TestBiogasFlex(unittest.TestCase):
    def setUp(self):
        self.start_date = "2017-01-01 00:00:00+0100"

    def test_step(self):
        params, inits = biogas_preset(555)
        params["sign_convention"] = "passive"
        biogas = BiogasPlant(params, inits)
        fcm = ForecastModel(biogas)
        fcm.set_now_dt(datetime.strptime(self.start_date, GER))
        fcm.set_step_size(15 * 60)
        fcm.set_p_kw(555)

        fcm.step()

        self.assertEqual(fcm.state.p_kw, 555)
        self.assertEqual(fcm.state.q_kvar, 0)

    def test_schedule(self):
        """This tests fails when forecast horizon is reduced from *2 to *1.
        Apparently, something related to get_state set_state does not work
        correctly.
        """
        params, inits = biogas_preset(555)
        params["sign_convention"] = "passive"
        biogas = BiogasPlant(params, inits)
        fcm = ForecastModel(biogas)
        now_dt = datetime.strptime(self.start_date, GER)

        expected = [
            0,
            55,
            125,
            180,
            250,
            277.5,
            305,
            375,
            430,
            500,
            555,
        ]

        schedule_df = pd.DataFrame(
            data=np.arange(0, 555 + 55.5, step=55.5),
            columns=["p_set_kw"],
            index=pd.date_range(now_dt, freq="15T", periods=11),
        )
        fcm.set_now_dt(now_dt)
        fcm.set_step_size(900)
        fcm.update_schedule(schedule_df)
        for setpoint in expected:
            fcm.set_now_dt(now_dt)
            fcm.set_step_size(15 * 60)
            fcm.step()
            now_dt += timedelta(seconds=900)
            self.assertEqual(-setpoint, fcm.get_p_kw())


if __name__ == "__main__":
    unittest.main()

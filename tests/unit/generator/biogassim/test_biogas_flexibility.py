"""This module contains tests for the biogas plant in combination
with the flexibility model.
"""
import unittest
from datetime import datetime

import pandas as pd
from pysimmods.generator.biogassim import BiogasPlant
from pysimmods.generator.biogassim.presets import biogas_preset
from pysimmods.other.flexibility.flexibility_model import (
    FlexibilityModel,
)
from pysimmods.util.date_util import GER


class TestBiogasFlex(unittest.TestCase):
    def setUp(self):
        self.start_date = "2016-12-31 23:00:00+0100"

    @unittest.skip
    def test_generate_schedules(self):
        flex = FlexibilityModel(BiogasPlant(*biogas_preset(1500)))

        index = pd.date_range(
            datetime.strptime(self.start_date, GER),
            freq="900S",
            periods=8,
        )
        schedule = pd.DataFrame(
            data=[0.25] * 8, columns=["target"], index=index
        )
        flex.update_schedule(schedule)
        flex.set_now_dt(self.start_date)
        flex.set_step_size(15 * 60)
        flex.step()

        flex.generate_schedules(
            self.start_date,
            flexibility_horizon_hours=24,
            num_schedules=5,
        )

        # try:
        #     import matplotlib.pyplot as plt

        #     plt.rcParams["figure.figsize"] = (12, 6)
        #     for key, schedule in flex.flexibilities.items():
        #         plt.plot(schedule["p_kw"], label=f"flex-{key}")
        #     plt.legend()
        #     plt.savefig("test_flexibilities.png")

        #     plt.figure()

        #     for key, schedule in flex.flexibilities.items():
        #         plt.plot(schedule["target"], label=f"flex-{key}")
        #     plt.legend()
        #     plt.savefig("test_flex_targets.png")
        # except ImportError:
        #     # Matplotlib not installed
        #     pass

        for key, schedule in flex.flexibilities.items():
            schedule.to_csv(f"test_flexibility_{key}.csv")

        self.assertIsInstance(flex.flexibilities, dict)
        self.assertEqual(len(flex.flexibilities), 5)
        for key, schedule in flex.flexibilities.items():
            self.assertIsInstance(schedule, pd.DataFrame)
            self.assertIn("target", schedule)
            self.assertIn("p_kw", schedule)
            self.assertIn("q_kvar", schedule)
            self.assertEqual(schedule.shape[0], 96)
            self.assertEqual(schedule.size, 3 * 96)


if __name__ == "__main__":
    unittest.main()

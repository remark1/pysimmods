"""Tests for the Biogas system together with the mosaik interface."""
import unittest

try:
    from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator
except ModuleNotFoundError:
    print("mosaik_api not found")
    pass

from pysimmods.generator.biogassim import BiogasPlant
from pysimmods.generator.biogassim.presets import biogas_preset


class TestSimulator(unittest.TestCase):
    def setUp(self):
        self.sim = PysimmodsSimulator()
        self.sim.init(
            sid="TestSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0000",
        )

        self.inputs = {
            "Biogas-0": {},
            "Biogas-1": {"p_set_mw": {"DummySim-0.DummyMod-0": 0.42}},
            "Biogas-2": {"p_set_kw": {"DummySim-0.DummyMod-1": 1512.5}},
        }

    def test_create_biogas(self):
        params, inits = biogas_preset(80)
        params["sign_convention"] = "passive"
        entities = self.sim.create(
            num=2,
            model="Biogas",
            params=params,
            inits=inits,
        )

        self.assertEqual(len(entities), 2)
        for entity in entities:
            self.assertIsInstance(entity, dict)
            self.assertIsInstance(self.sim.models[entity["eid"]], BiogasPlant)

    def test_step(self):

        params, inits = biogas_preset(80)
        params["sign_convention"] = "passive"
        self.sim.create(
            num=1,
            model="Biogas",
            params=params,
            inits=inits,
        )
        params, inits = biogas_preset(555)
        params["sign_convention"] = "passive"
        self.sim.create(
            num=1,
            model="Biogas",
            params=params,
            inits=inits,
        )

        params, inits = biogas_preset(2050)
        params["sign_convention"] = "passive"
        self.sim.create(
            num=1,
            model="Biogas",
            params=params,
            inits=inits,
        )

        self.sim.step(0, self.inputs)
        # Default target is 60 kW, however the model is only able to
        # provide 40 or 80 and will use the higher one
        self.assertEqual(self.sim.models["Biogas-0"].get_p_kw(), -80)
        self.assertEqual(self.sim.models["Biogas-1"].get_p_kw(), -430)
        self.assertEqual(self.sim.models["Biogas-2"].get_p_kw(), -1600)

    def test_get_data(self):
        params, inits = biogas_preset(80)
        params["sign_convention"] = "passive"
        self.sim.create(
            num=1,
            model="Biogas",
            params=params,
            inits=inits,
        )
        params, inits = biogas_preset(555)
        params["sign_convention"] = "passive"
        self.sim.create(
            num=1,
            model="Biogas",
            params=params,
            inits=inits,
        )

        params, inits = biogas_preset(2050)
        params["sign_convention"] = "passive"
        self.sim.create(
            num=1,
            model="Biogas",
            params=params,
            inits=inits,
        )

        self.sim.step(0, self.inputs)
        outputs = {
            "Biogas-0": ["p_mw", "q_mvar"],
            "Biogas-1": ["p_mw", "q_mvar"],
            "Biogas-2": ["p_mw", "q_mvar"],
        }

        data = self.sim.get_data(outputs)
        num_vals = 0
        self.assertIsInstance(data, dict)
        for key, value in data.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, dict)

            for attr, val in value.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 6)

        # From outside, mw is used
        self.assertEqual(data["Biogas-0"]["p_mw"], -0.08)
        self.assertEqual(data["Biogas-0"]["q_mvar"], 0.0)
        self.assertEqual(data["Biogas-1"]["p_mw"], -0.43)
        self.assertEqual(data["Biogas-1"]["q_mvar"], 0.0)
        self.assertEqual(data["Biogas-2"]["p_mw"], -1.6)
        self.assertEqual(data["Biogas-2"]["q_mvar"], 0.0)


if __name__ == "__main__":
    unittest.main()

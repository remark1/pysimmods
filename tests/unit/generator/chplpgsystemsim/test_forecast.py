import unittest
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from pysimmods.generator.chplpgsystemsim import CHPLPGSystem
from pysimmods.generator.chplpgsystemsim.presets import chp_preset
from pysimmods.other.flexibility.forecast_model import ForecastModel
from pysimmods.util.date_util import GER


class TestCHPSystemForecast(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-02-21 08:00:00+0100"
        self.forecasts = pd.DataFrame(
            data=np.ones(15) * 15,
            columns=["day_avg_t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                periods=15,
                freq="15T",
            ),
        )
        self.params, self.inits = chp_preset(400, self.start_date)
        self.params["sign_convention"] = "passive"

    def test_step(self):
        chp = CHPLPGSystem(self.params, self.inits)
        fcm = ForecastModel(CHPLPGSystem(self.params, self.inits))

        chp.set_now_dt(self.start_date)
        chp.set_step_size(15 * 60)
        chp.set_p_kw(300)
        chp.inputs.day_avg_t_air_deg_celsius = 15
        chp.step()

        fcm.update_forecasts(self.forecasts)
        fcm.set_now_dt(self.start_date)
        fcm.set_step_size(15 * 60)
        fcm.set_p_kw(300)
        fcm.inputs.day_avg_t_air_deg_celsius = 15
        fcm.step()

        self.assertEqual(chp.get_p_kw(), fcm.get_p_kw())

    def test_schedule(self):
        now_dt = datetime.strptime(self.start_date, GER)
        fcm = ForecastModel(
            CHPLPGSystem(self.params, self.inits), step_size=900, now_dt=now_dt
        )

        expected = [
            0,
            200,
            200,
            200,
            200,
            200,
            240,
            280,
            320,
            360,
            400,
        ]
        schedule_df = pd.DataFrame(
            data=np.arange(0, 440, step=40),
            columns=["p_set_kw"],
            index=pd.date_range(now_dt, periods=11, freq="15T"),
        )

        fcm.update_schedule(schedule_df)
        fcm.update_forecasts(self.forecasts)

        for _, setpoint in enumerate(expected):
            fcm.set_now_dt(now_dt)
            fcm.set_step_size(15 * 60)
            fcm.inputs.day_avg_t_air_deg_celsius = 15.0
            fcm.step()

            now_dt += timedelta(seconds=900)
            # print(-setpoint, fcm.get_p_kw())
            self.assertEqual(-setpoint, fcm.get_p_kw())


if __name__ == "__main__":
    unittest.main()

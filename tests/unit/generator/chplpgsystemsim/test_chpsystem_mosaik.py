import sys
import unittest

try:
    from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator
except ModuleNotFoundError:
    print("mosaik_api not found")
    pass

from pysimmods.generator.chplpgsystemsim.presets import chp_preset


class TestCHPSystemMosaik(unittest.TestCase):
    @unittest.skipIf(
        "mosaik_api" not in sys.modules,
        "mosaik_api is not installed.",
    )
    def test_get_data(self):
        sim = PysimmodsSimulator()
        sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2019-08-11 04:00:00+0100",
        )
        inputs = {
            "CHP-0": {
                "day_avg_t_air_deg_celsius": {"DummySim-0.DummyMod-0": 6.0},
                "p_set_kw": {"DummySim-0.DummyMod-0": 7},
            },
            "CHP-1": {
                "e_th_demand_set_kwh": {"DummySim-0.DummyMod-0": -3.0},
            },
        }
        params, inits = chp_preset(7)
        sim.create(num=2, model="CHP", params=params, inits=inits)

        sim.step(0, inputs)

        outputs = {
            "CHP-0": ["p_mw", "q_mvar"],
            "CHP-1": ["p_mw", "q_mvar", "p_th_kw", "e_th_demand_kwh"],
        }

        data = sim.get_data(outputs)
        num_vals = 0
        self.assertIsInstance(data, dict)
        for key, value in data.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, dict)

            for attr, val in value.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 6)


if __name__ == "__main__":
    unittest.main()

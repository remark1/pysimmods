import copy
import unittest

from pysimmods.generator.pvsim.pvp import PhotovoltaicPowerPlant


class TestPV(unittest.TestCase):
    def setUp(self):
        self.params = {
            "lat_deg": 48.40,
            "lon_deg": 9.98,
            "orient_deg": 180.0,
            "tilt_deg": 30.0,
            "eta_percent": 15,
            "beta_percent_per_kelvin": 0.29,
            "k_m_w_per_m2": 50.0,
            "a_m2": 15,
            "alpha_w_per_m2_kelvin": 11.0,
            "rho_kg_per_m2": 15.0,
            "reflex_percent": 5,
            "c_j_per_kg_kelvin": 900.0,
        }
        self.inits = {
            "t_module_deg_celsius": 25,
        }

    def test_step(self):

        # Instantiate new PV model
        pv_plant = PhotovoltaicPowerPlant(self.params, self.inits)

        # Set inputs
        pv_plant.set_step_size(5 * 60)
        pv_plant.set_now_dt("2006-04-15 12:00:00+0000")

        # Set air temerature in °C
        pv_plant.inputs.t_air_deg_celsius = 25

        # Set direct radiation on horizontal plane in W/m2
        pv_plant.inputs.bh_w_per_m2 = 400
        # Set diffuse radiation on horizontal plane in W/m2
        pv_plant.inputs.dh_w_per_m2 = 200

        self.assertEqual(pv_plant.state.t_module_deg_celsius, 25.0)

        # Perform simulation step
        pv_plant.step()

        self.assertAlmostEqual(pv_plant.get_p_kw(), -1.5831096)
        self.assertAlmostEqual(pv_plant.state.t_module_deg_celsius, 38.2207168)

    def test_radiation(self):
        self.params["has_external_irradiance_model"] = True
        pv_plant = PhotovoltaicPowerPlant(self.params, self.inits)

        pv_plant.set_step_size(5 * 60)
        pv_plant.inputs.s_module_w_per_m2 = 1000
        pv_plant.inputs.t_air_deg_celsius = 20

        pv_plant.step()

        self.assertAlmostEqual(pv_plant.get_p_kw(), -2.141975)
        self.assertAlmostEqual(pv_plant.state.t_module_deg_celsius, 41.5555556)

    def test_state(self):
        pv_plant = PhotovoltaicPowerPlant(self.params, self.inits)

        state_dict = pv_plant.get_state()
        saved_state = copy.deepcopy(pv_plant.state)

        pv_plant.set_step_size(5 * 60)
        pv_plant.set_now_dt("2006-08-01 12:00:00+0000")
        pv_plant.inputs.bh_w_per_m2 = 500
        pv_plant.inputs.dh_w_per_m2 = 300
        pv_plant.inputs.t_air_deg_celsius = 20

        pv_plant.step()
        pv_plant.set_state(state_dict)

        self.assertEqual(saved_state.p_kw, pv_plant.state.p_kw)
        self.assertEqual(
            saved_state.t_module_deg_celsius,
            pv_plant.state.t_module_deg_celsius,
        )


if __name__ == "__main__":
    unittest.main()

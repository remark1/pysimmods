import os
import numpy as np
import matplotlib.pyplot as plt

from pysimmods.buffer.batterysim import Battery
from pysimmods.other.flexibility.flexibility_model import (
    FlexibilityModel,
)


class TestBatteryExtras:
    def __init__(self):
        self.params = {
            "cap_kwh": 5,
            "soc_min_percent": 15,
            "p_charge_max_kw": 1,
            "p_discharge_max_kw": 1,
            "eta_pc": [-2.109566, 0.403556, 97.110770],
        }
        self.inits = {
            "soc_percent": 50,
        }
        self.start_date = "2018-05-19 00:00:00+0100"

    def test_default_schedule(self):
        bat = Battery(self.params, self.inits)
        bat = FlexibilityModel(bat, self.start_date, 900)

        steps = np.arange(1 * 24 * 4)
        p_kws = list()
        socs = np.zeros_like(steps)

        for idx in steps:
            print(f"Processing step {idx} ...", end="\r")
            bat.step()
            p_kws.append(bat.state.p_kw)
            socs[idx] = bat.state.soc_percent

        print("Done stepping")
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))

        ax1.plot(steps, p_kws, label="power [kW]")
        ax2.plot(steps, socs, label="state of charge")
        print(bat.state.soc_percent)
        plt.legend()
        plt.show()


def main():
    tbe = TestBatteryExtras()
    tbe.test_default_schedule()


if __name__ == "__main__":
    main()

"""Tests for the battery model."""

import unittest
from datetime import datetime, timedelta, timezone

from pysimmods.buffer.batterysim.battery import Battery


class TestBattery(unittest.TestCase):
    def setUp(self):
        self.params = {
            "cap_kwh": 5,
            "soc_min_percent": 15,
            "p_charge_max_kw": 1,
            "p_discharge_max_kw": 1,
            "eta_pc": [-2.109566, 0.403556, 97.110770],
        }
        self.inits = {
            "soc_percent": 50,
        }

    def test_step_fully_charged(self):
        """Test if power is reduced in last step before battery is
        fully loaded

        """
        battery = Battery(self.params, self.inits)
        schedule = [1] * 31
        for p_set_kw in schedule:
            battery.set_step_size(5 * 60)
            battery.set_p_kw(p_set_kw)
            battery.step()

        self.assertLess(battery.state.p_kw, p_set_kw)

    def test_soc_falls_below_soc_min(self):
        """Check that state of charge doesn't fall below minimum
        state of charge

        """
        battery = Battery(self.params, self.inits)
        schedule = [-1] * 31
        for p_set_kw in schedule:
            battery.set_step_size(5 * 60)
            battery.set_p_kw(p_set_kw)
            battery.step()
            self.assertLessEqual(battery.get_p_kw(), 0)
            # print(battery.get_p_kw(), battery.state.soc_percent)

        self.assertGreaterEqual(battery.state.soc_percent, 15)

    def test_num_steps_to(self):
        """Test how many steps are required to fully charge and
        discharge the battery.

        Showcases the behavior for battery with a capacity:charge-rate
        ratio of 5:1.
        """
        self.params["sign_convention"] = "passive"
        battery = Battery(self.params, {"soc_percent": 15})

        minutes = 0
        while battery.state.soc_percent < 100:
            battery.set_p_kw(1.0)
            battery.set_step_size(60)
            battery.step()
            minutes += 1

        # Should be fully charged after 263 "minutes"
        self.assertEqual(minutes, 263)
        fully_charged = battery.get_state()

        minutes = 0

        while battery.state.soc_percent > 15:
            battery.set_p_kw(-1.0)
            battery.set_step_size(60)
            battery.step()
            minutes += 1

        # Should be fully discharged after 248 "minutes"
        self.assertEqual(minutes, 248)

        minutes = 0
        while battery.state.soc_percent < 50:
            battery.set_p_kw(1.0)
            battery.set_step_size(60)
            battery.step()
            minutes += 1

        # Should be half-charged after 109 "minutes"
        self.assertEqual(minutes, 109)
        minutes = 0

        battery.set_state(fully_charged)
        while battery.state.soc_percent > 50:
            battery.set_p_kw(-1.0)
            battery.set_step_size(60)
            battery.step()
            minutes += 1

        # Should be half-discharged after 146 "minutes"
        self.assertEqual(minutes, 146)

    def test_default_schedule(self):
        """Test if the default schedule is used.

        This required to set the now_dt before each step.
        """
        battery = Battery(self.params, self.inits)
        now_dt = datetime(2021, 12, 7, 8, 0, 0, tzinfo=timezone.utc)
        expected = [0, 0, -0.3, -0.3, -0.3, -0.3, -0.3, -0.3]
        for idx in range(8):
            battery.set_step_size(1800)
            battery.set_now_dt(now_dt)
            battery.step()

            self.assertAlmostEqual(expected[idx], battery.state.p_kw)

            now_dt += timedelta(seconds=1800)

    def test_discharge(self):
        bat1 = Battery(self.params, self.inits)
        bat1.state.soc_percent = 15.61658
        bat1.inputs.step_size = 900
        bat1.inputs.p_set_kw = -1

        bat2 = Battery(self.params, self.inits)
        bat2.state.soc_percent = 57.33569
        bat2.inputs.step_size = 900
        bat2.inputs.p_set_kw = -1

        bat1.step()
        bat2.step()

        self.assertLessEqual(bat1.get_p_kw(), 0)
        self.assertNotEqual(bat1.get_p_kw(), bat2.get_p_kw())


if __name__ == "__main__":
    unittest.main()

"""Testcase for the flex model used with the battery model."""
import unittest

import pandas as pd
from pysimmods.buffer.batterysim import Battery
from pysimmods.buffer.batterysim.presets import battery_preset
from pysimmods.other.flexibility.flexibility_model import (
    FlexibilityModel,
)


class TestBatteryFlexibility(unittest.TestCase):
    def setUp(self):
        self.start_date = "2018-05-19 14:00:00+0100"

    @unittest.skip
    def test_generate_schedules(self):
        """Test the flexibility provision of the battery model."""
        flex = FlexibilityModel(Battery(*battery_preset(5)))
        flex.set_now_dt(self.start_date)
        flex.set_step_size(15 * 60)
        flex.step()

        flexis = flex.generate_schedules("2018-05-19 15:00:00+0100", 2, 10)

        self.assertIsInstance(flexis, dict)
        self.assertEqual(len(flexis), 10)

        for _, val in flexis.items():
            self.assertIsInstance(val, pd.DataFrame)
            self.assertEqual(val.columns[0], "target")
            self.assertEqual(val.columns[1], "p_kw")
            for target in val["target"].values:
                self.assertTrue(0 <= target < 100)
            for actual in val["p_kw"].values:
                self.assertTrue(-5 <= actual <= 5)


if __name__ == "__main__":
    unittest.main()

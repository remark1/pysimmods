import unittest

from pysimmods.consumer.hvacsim import HVAC
from pysimmods.consumer.hvacsim.presets import (
    hvac_1279kw_init,
    hvac_1279kw_params,
)


class TestHVAC(unittest.TestCase):
    def test_fridge(self):

        params = {
            "p_max_kw": 0.08,
            "eta_percent": 200.0,
            "l_m": 0.5,
            "w_m": 0.5,
            "h_m": 0.5,
            "d_m": 0.02,
            "lambda_w_per_m_k": 0.019,
            "t_min_deg_celsius": 3.0,
            "t_max_deg_celsius": 7.0,
        }
        init_vals = {
            "mass_kg": 5.0,
            "c_j_per_kg_k": 3.18 * 1e3,
            "theta_t_deg_celsius": 4.0,
            "cooling": True,
            "mode": "auto",
        }

        fridge = HVAC(params, init_vals)

        fridge.inputs.t_air_deg_celsius = 20.0
        fridge.set_p_kw(0.08)
        fridge.set_step_size(60)

        fridge.step()

        self.assertEqual(fridge.state.p_kw, 0.08)
        self.assertAlmostEqual(fridge.state.theta_t_deg_celsius, 3.4822642)

    def test_hvac(self):
        params = {
            "p_max_kw": 2.0,
            "eta_percent": 200.0,
            "l_m": 4.0,
            "w_m": 5.0,
            "h_m": 2.5,
            "d_m": 0.25,
            "lambda_w_per_m_k": 0.5,
            "t_min_deg_celsius": 17.0,
            "t_max_deg_celsius": 23.0,
        }
        init_vals = {
            "mass_kg": 500.0,
            "c_j_per_kg_k": 2390.0,
            "theta_t_deg_celsius": 21.0,
            "cooling": True,
            "mode": "auto",
        }
        hvac = HVAC(params, init_vals)

        hvac.inputs.t_air_deg_celsius = 28.0
        hvac.set_p_kw(2.0)
        hvac.set_step_size(900)

        hvac.step()

        self.assertEqual(hvac.state.p_kw, 2.0)
        self.assertAlmostEqual(hvac.state.theta_t_deg_celsius, 18.883682)

    def test_num_steps_to(self):
        params = {
            "p_max_kw": 2.0,
            "eta_percent": 200.0,
            "l_m": 4.0,
            "w_m": 5.0,
            "h_m": 2.5,
            "d_m": 0.25,
            "lambda_w_per_m_k": 0.5,
            "t_min_deg_celsius": 17.0,
            "t_max_deg_celsius": 23.0,
        }
        init_vals = {
            "mass_kg": 500.0,
            "c_j_per_kg_k": 2390.0,
            "theta_t_deg_celsius": 23.0,
            "cooling": True,
            "mode": "auto",
        }
        hvac = HVAC(params, init_vals)

        minutes = 0
        while hvac.state.theta_t_deg_celsius > 17.0:
            hvac.inputs.t_air_deg_celsius = 28
            hvac.set_step_size(60)
            hvac.set_p_kw(1.5)
            hvac.step()
            minutes += 1

        self.assertEqual(minutes, 75)

        minutes = 0
        while hvac.state.theta_t_deg_celsius < 23.0:
            hvac.inputs.t_air_deg_celsius = 28
            hvac.set_step_size(60)
            hvac.set_p_kw(0.2)
            hvac.step()
            minutes += 1

        self.assertEqual(minutes, 139)

    def test_large_hvac(self):
        hvac = HVAC(hvac_1279kw_params(), hvac_1279kw_init())
        minutes = 0
        while hvac.state.theta_t_deg_celsius > -30:
            hvac.inputs.t_air_deg_celsius = 16.0
            hvac.set_step_size(60)
            hvac.set_p_kw(1279.0)
            hvac.step()
            minutes += 1

        self.assertEqual(minutes, 1551)  # realism?

        minutes = 0
        while hvac.state.theta_t_deg_celsius < -18:
            hvac.inputs.t_air_deg_celsius = 16.0
            hvac.set_step_size(60)
            hvac.set_p_kw(0)
            hvac.step()
            minutes += 1

        self.assertEqual(minutes, 2727)  # realism?

    def test_sign(self):
        params = {
            "p_max_kw": 2.0,
            "eta_percent": 200.0,
            "l_m": 4.0,
            "w_m": 5.0,
            "h_m": 2.5,
            "d_m": 0.25,
            "lambda_w_per_m_k": 0.5,
            "t_min_deg_celsius": 17.0,
            "t_max_deg_celsius": 23.0,
            "sign_convention": "passive",
        }
        init_vals = {
            "mass_kg": 500.0,
            "c_j_per_kg_k": 2390.0,
            "theta_t_deg_celsius": 23.0,
            "cooling": True,
            "mode": "auto",
        }
        hvac_pas = HVAC(params, init_vals)
        params["sign_convention"] = "active"
        hvac_act = HVAC(params, init_vals)

        hvac_pas.inputs.t_air_deg_celsius = 28
        hvac_pas.set_step_size(60)
        hvac_pas.set_p_kw(1.5)
        hvac_pas.step()

        hvac_act.inputs.t_air_deg_celsius = 28
        hvac_act.set_step_size(60)
        hvac_act.set_p_kw(1.5)
        hvac_act.step()

        self.assertEqual(hvac_pas.state.p_kw, hvac_act.state.p_kw)
        self.assertEqual(hvac_pas.get_p_kw(), -hvac_act.get_p_kw())
        self.assertEqual(
            hvac_pas.state.theta_t_deg_celsius,
            hvac_act.state.theta_t_deg_celsius,
        )

        hvac_pas.inputs.t_air_deg_celsius = 28
        hvac_pas.set_step_size(60)
        hvac_pas.set_p_kw(2)
        hvac_pas.step()

        hvac_act.inputs.t_air_deg_celsius = 28
        hvac_act.set_step_size(60)
        hvac_act.set_p_kw(-2)
        hvac_act.step()

        self.assertEqual(hvac_pas.state.p_kw, hvac_act.state.p_kw)
        self.assertEqual(hvac_pas.get_p_kw(), -hvac_act.get_p_kw())
        self.assertEqual(
            hvac_pas.state.theta_t_deg_celsius,
            hvac_act.state.theta_t_deg_celsius,
        )

        hvac_pas.inputs.t_air_deg_celsius = 28
        hvac_pas.set_step_size(60)
        hvac_pas.set_p_kw(-2)
        hvac_pas.step()

        self.assertEqual(2.0, hvac_pas.get_p_kw())


if __name__ == "__main__":
    unittest.main()

import unittest
from datetime import datetime, timedelta

from pysimmods.consumer.hvacsim import HVAC
from pysimmods.consumer.hvacsim.presets import (
    hvac_1279kw_params,
    hvac_1279kw_init,
)
from pysimmods.other.flexibility.schedule import Schedule
from pysimmods.other.flexibility.schedule_model import ScheduleModel
from pysimmods.util.date_util import GER


class TestHVACSchedule(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-12-14 00:00:00+0000"
        self.now_dt = datetime.strptime(self.start_date, GER)
        self.step_size = 900

    def test_schedule_operation(self):
        model = HVAC(hvac_1279kw_params(), hvac_1279kw_init())
        smodel = ScheduleModel(HVAC(hvac_1279kw_params(), hvac_1279kw_init()))
        schedule = Schedule(self.step_size, 1, self.now_dt)
        schedule.init()
        now_dt = self.now_dt
        for _ in range(80):
            schedule.update_entry(now_dt, "p_set_kw", 575.55)
            now_dt += timedelta(seconds=self.step_size)

        smodel.set_now_dt(now_dt)
        smodel.set_step_size(900)
        smodel.update_schedule(schedule)
        now_dt = self.now_dt
        for idx in range(96):
            model.set_now_dt(now_dt)
            model.set_step_size(900)
            model.inputs.t_air_deg_celsius = 28
            smodel.set_now_dt(now_dt)
            smodel.set_step_size(900)
            smodel.inputs.t_air_deg_celsius = 28
            model.step()
            smodel.step()

            now_dt += timedelta(seconds=self.step_size)
            s_p_kw = smodel.get_p_kw()
            if idx < 80:
                # Schedule active
                if s_p_kw != 0:
                    self.assertNotEqual(s_p_kw, model.get_p_kw())
                self.assertGreaterEqual(s_p_kw, 0)


if __name__ == "__main__":
    unittest.main()

import unittest

from pysimmods.other.heatdemandsim import heatdemand


class TestHeatDemand(unittest.TestCase):
    def init(self, p_th_prod_kw=-220):
        params = {"chp_p_th_prod_kw": p_th_prod_kw}
        inits = {
            "start_date": "2017-01-01 23:00:00+0100",
            "t_last_3_deg_celsius": 7.1,
            "t_last_2_deg_celsius": 6.7,
            "t_last_1_deg_celsius": 8.2,
        }
        self.sim = heatdemand.create_heatdemand(params, inits)

    def test_init(self):
        self.init()
        self.assertIsInstance(self.sim, heatdemand.HeatDemand)

    def test_find_one_family(self):

        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(0.001)
        self.assertEqual(hp["name"], "one_family")

        hp = heatdemand.find_demand_profile(9)
        self.assertEqual(hp["name"], "one_family")

    def test_find_store(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(10)
        self.assertEqual(hp["name"], "store")

        hp = heatdemand.find_demand_profile(15)
        self.assertEqual(hp["name"], "store")

    def test_find_multi_family(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(16)
        self.assertEqual(hp["name"], "multi_family")

        hp = heatdemand.find_demand_profile(24)
        self.assertEqual(hp["name"], "multi_family")

    def test_find_laundry(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(25)
        self.assertEqual(hp["name"], "laundry")

        hp = heatdemand.find_demand_profile(60)
        self.assertEqual(hp["name"], "laundry")

    def test_find_metal_kfz(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(61)
        self.assertEqual(hp["name"], "metal_kfz")

        hp = heatdemand.find_demand_profile(132)
        self.assertEqual(hp["name"], "metal_kfz")

    def test_find_paper_print(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(133)
        self.assertEqual(hp["name"], "paper_print")

        hp = heatdemand.find_demand_profile(198)
        self.assertEqual(hp["name"], "paper_print")

    def test_find_bakery(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(211)
        self.assertEqual(hp["name"], "bakery")

        hp = heatdemand.find_demand_profile(347)
        self.assertEqual(hp["name"], "bakery")

    def test_find_restaurant(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(348)
        self.assertEqual(hp["name"], "restaurant")

        hp = heatdemand.find_demand_profile(598)
        self.assertEqual(hp["name"], "restaurant")

    def test_find_business_swimmingpool(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(599)
        self.assertEqual(hp["name"], "business_swimmingpool")

        hp = heatdemand.find_demand_profile(949)
        self.assertEqual(hp["name"], "business_swimmingpool")

    def test_find_accommodation(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(950)
        self.assertEqual(hp["name"], "accommodation")

        hp = heatdemand.find_demand_profile(1310)
        self.assertEqual(hp["name"], "accommodation")

    def test_find_accommodation_hospital(self):
        heatdemand.load_demand_profiles()

        hp = heatdemand.find_demand_profile(1540)
        self.assertEqual(hp["name"], "accommodation_hospital")

        hp = heatdemand.find_demand_profile(100000)
        self.assertEqual(hp["name"], "accommodation_hospital")

    def test_step(self):

        self.init()
        self.sim.inputs.step_size = 900
        self.sim.inputs.day_avg_t_air_deg_celsius = 11.5

        self.sim.step()

        self.assertAlmostEqual(self.sim.state.e_th_kwh, 39.6217979)


if __name__ == "__main__":
    unittest.main()

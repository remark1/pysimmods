"""This module contains the test cases for the inverter."""
import unittest

from pysimmods.other.invertersim import Inverter


class TestInverter(unittest.TestCase):
    """Test class for the inverter.

    All tests use inductive reactive power and the load reference
    system. Changing one of them just flips the sign.

    """

    def setUp(self):
        self.params = {
            "sn_kva": 20,
            "q_control": "prioritize_p",
            "cos_phi": 0.9,
            "inverter_mode": "inductive",
            "sign_convention": "passive",
        }

    def test_no_setpoint_inductive(self):
        inv = Inverter(self.params, {})

        inv.inputs.p_in_kw = 10

        inv.step()

        self.assertEqual(-10, inv.get_p_kw())
        self.assertAlmostEqual(4.8432210, inv.get_q_kvar())
        self.assertEqual(0.9, inv.get_cos_phi())

    def test_no_setpoint_capacitive(self):
        self.params["inverter_mode"] = "capacitive"
        inv = Inverter(self.params, {})

        inv.inputs.p_in_kw = 10

        inv.step()

        self.assertEqual(-10, inv.get_p_kw())
        self.assertAlmostEqual(-4.8432210, inv.get_q_kvar())
        self.assertEqual(0.9, inv.get_cos_phi())

    def test_no_setpoint_inductive_active(self):
        self.params["sign_convention"] = "active"
        inv = Inverter(self.params, {})

        inv.inputs.p_in_kw = 10

        inv.step()

        self.assertEqual(10, inv.get_p_kw())
        self.assertAlmostEqual(-4.8432210, inv.get_q_kvar())
        self.assertEqual(0.9, inv.get_cos_phi())

    def test_no_setpoint_capacitive_active(self):
        self.params["sign_convention"] = "active"
        self.params["inverter_mode"] = "capacitive"
        inv = Inverter(self.params, {})

        inv.inputs.p_in_kw = 10

        inv.step()

        self.assertEqual(10, inv.get_p_kw())
        self.assertAlmostEqual(4.8432210, inv.get_q_kvar())
        self.assertEqual(0.9, inv.get_cos_phi())

    def test_no_setpoint_limited_s_kva(self):
        inv = Inverter(self.params, {})

        inv.inputs.p_in_kw = 19.5

        inv.step()

        # Keep cos phi constant
        self.assertEqual(-18.0, inv.get_p_kw())
        self.assertAlmostEqual(8.7177979, inv.get_q_kvar())
        self.assertEqual(0.9, inv.get_cos_phi())

    def test_p_setpoint(self):
        inv = Inverter(self.params, {})
        inv.inputs.p_in_kw = 10
        inv.inputs.p_set_kw = -5

        inv.step()

        self.assertEqual(-5, inv.get_p_kw())
        self.assertAlmostEqual(2.4216105, inv.get_q_kvar())
        self.assertEqual(0.9, inv.get_cos_phi())

    def test_q_setpoint(self):
        inv = Inverter(self.params, {})
        inv.inputs.p_in_kw = 10
        inv.inputs.q_set_kvar = 3

        inv.step()

        self.assertEqual(-10, inv.get_p_kw())
        self.assertAlmostEqual(-3, inv.get_q_kvar())
        self.assertAlmostEqual(0.9578263, inv.get_cos_phi())

        inv.inputs.p_in_kw = 18
        inv.inputs.q_set_kvar = -3

        inv.step()

        self.assertEqual(-18, inv.get_p_kw())
        self.assertAlmostEqual(3, inv.get_q_kvar())
        self.assertAlmostEqual(0.9863939, inv.get_cos_phi())

        inv.inputs.p_in_kw = 18
        inv.inputs.q_set_kvar = 18

        inv.step()

        # Q gets cut off
        self.assertEqual(-18, inv.get_p_kw())
        self.assertAlmostEqual(-8.7177979, inv.get_q_kvar())
        self.assertAlmostEqual(0.7071068, inv.get_cos_phi())

    def test_q_setpoint_active(self):
        self.params["sign_convention"] = "active"
        inv = Inverter(self.params, {})
        inv.inputs.p_in_kw = 10
        inv.inputs.q_set_kvar = 3

        inv.step()

        self.assertEqual(10, inv.get_p_kw())
        self.assertAlmostEqual(3, inv.get_q_kvar())
        self.assertAlmostEqual(0.9578263, inv.get_cos_phi())

        inv.inputs.p_in_kw = 18
        inv.inputs.q_set_kvar = -3

        inv.step()

        self.assertEqual(18, inv.get_p_kw())
        self.assertAlmostEqual(-3, inv.get_q_kvar())
        self.assertAlmostEqual(0.9863939, inv.get_cos_phi())

        inv.inputs.p_in_kw = 18
        inv.inputs.q_set_kvar = 18

        inv.step()

        # Q gets cut off
        self.assertEqual(18, inv.get_p_kw())
        self.assertAlmostEqual(8.7177979, inv.get_q_kvar())
        self.assertAlmostEqual(0.7071068, inv.get_cos_phi())


if __name__ == "__main__":
    unittest.main()

"""This module contains the tests for the flexibility model."""
import unittest

import pandas as pd
from pysimmods.other.dummy.model import DummyModel
from pysimmods.other.flexibility.flexibility_model import (
    FlexibilityModel,
)


class TestFlexibilityModel(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-01-06 12:00:00+0100"
        self.flex_start = "2021-01-06 12:30:00+0100"
        self.step_size = 900
        self.model = DummyModel(dict(), dict())

    @unittest.skip
    def test_generate_schedules(self):
        """Test the generate schedules functionality."""

        flex = FlexibilityModel(DummyModel(dict(), dict()))

        flex.set_now_dt(self.start_date)
        flex.set_step_size(15 * 60)
        flex.set_percent(50)

        flex.step()

        flexis = flex.generate_schedules(self.flex_start, 2, 10)
        self.assertIsInstance(flexis, dict)
        self.assertEqual(len(flexis), 10)

        for _, val in flexis.items():
            self.assertIsInstance(val, pd.DataFrame)
            self.assertEqual(val.columns[0], "target")
            self.assertEqual(val.columns[1], "p_kw")
            for target in val["target"].values:
                self.assertTrue(0 <= target < 100.0)
            for actual in val["p_kw"].values:
                self.assertTrue(0 <= actual <= 500)
                # Dummy Model p_max_kw = 500


if __name__ == "__main__":
    unittest.main()

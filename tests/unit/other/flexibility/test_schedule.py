import unittest
from datetime import datetime, timedelta, timezone

import numpy as np
import pandas as pd
from pysimmods.other.flexibility.schedule import Schedule


class TestSchedule(unittest.TestCase):
    def setUp(self):

        self.now_dt = datetime(2019, 8, 11, 3, 0, 0, tzinfo=timezone.utc)

        steps = 8
        index = pd.date_range(
            self.now_dt + timedelta(seconds=900), periods=steps, freq=f"{900}S"
        )

        self.other = pd.DataFrame(
            data=np.array(
                [
                    np.ones(steps),
                    np.zeros(steps),
                    np.ones(steps),
                    np.zeros(steps),
                ]
            ).transpose(),
            columns=["p_set_kw", "q_set_kvar", "p_kw", "q_kvar"],
            index=index,
        )

    def test_init_empty(self):
        """Test to initialize a schedule without any information."""

        schedule = Schedule()
        schedule.init()

        self.assertIn("p_set_kw", schedule._data.columns)
        self.assertIn("q_set_kvar", schedule._data.columns)
        self.assertIn("p_kw", schedule._data.columns)
        self.assertIn("q_kvar", schedule._data.columns)
        self.assertIsInstance(schedule._data.index, pd.DatetimeIndex)

    def test_init_with_start_date(self):
        """Test to initialize a schedule with start date and horizon
        and step size information."""

        schedule = Schedule(
            900,
            2,
            self.now_dt,
        )

        schedule.init()

        # 4 entries/hour x 2 hours x 4 columns
        num_entries_expected = 4 * 2 * 4
        self.assertEqual(schedule._data.size, num_entries_expected)
        self.assertIsInstance(schedule._data.index, pd.DatetimeIndex)

        schedule.use_absolute_setpoints = True
        schedule.init()

        # 4 entries/hour x 2 hours x 4 columns
        num_entries_expected = 4 * 2 * 4
        self.assertEqual(schedule._data.size, num_entries_expected)

    def test_update(self):
        """Test the update method of the schedule.

        The new schedule consists of 8 steps, starting on the next
        step. After the update, the schedules data frame should consist
        of nine steps, with the first one consisting of np.nan values
        and all subsequent rows have the updated values from the new
        schedule.
        """
        schedule = Schedule(900, 1, self.now_dt)
        schedule.init()

        # 4 entries/hour x 1 hours x 4 columns
        num_entries_expected = 4 * 1 * 4
        self.assertEqual(schedule._data.size, num_entries_expected)

        schedule.update(self.other)

        # 4 entries/hour x 2.25 hours x 4 columns
        num_entries_expected = int(4 * 2.25 * 4)
        self.assertEqual(schedule._data.size, num_entries_expected)

    def test_update_from_different_timezone(self):
        """Test if the update method still works when the other
        schedule uses a different timezone.
        """
        steps = 8
        now_dt = datetime(
            2019, 8, 11, 4, 0, 0, tzinfo=timezone(timedelta(hours=1))
        )
        index = pd.date_range(
            now_dt + timedelta(seconds=900), periods=steps, freq=f"{900}S"
        )

        self.other.index = pd.to_datetime(index)

        schedule = Schedule(900, 1, self.now_dt)
        schedule.init()
        schedule.update(self.other)

        self.assertIsInstance(schedule._data.index, pd.DatetimeIndex)

        t_ns = [schedule.now_dt]
        for _ in range(steps):
            t_ns.append(t_ns[-1] + timedelta(seconds=schedule.step_size))

        for idx, t_n in enumerate(t_ns):
            self.assertTrue(schedule.has_index(t_n))
            if idx == 0:
                self.assertIsNone(schedule.get(t_n, "p_set_kw"))
                self.assertIsNone(schedule.get(t_n, "q_set_kvar"))
                self.assertIsNone(schedule.get(t_n, "p_kw"))
                self.assertIsNone(schedule.get(t_n, "q_kvar"))
            else:
                self.assertEqual(schedule.get(t_n, "p_set_kw"), 1.0)
                self.assertEqual(schedule.get(t_n, "q_set_kvar"), 0.0)
                self.assertEqual(schedule.get(t_n, "p_kw"), 1.0)
                self.assertEqual(schedule.get(t_n, "q_kvar"), 0.0)

    def test_update_column_subset(self):
        """Test update if only a subset of the columns is present.

        Expected outcome:
        The available columns shall be updated, missing values are
        filled with NaN.

        """

    def test_update_to_many_columns(self):
        """Test update if more than the required columns are present.

        Expected outcome:
        Only the columns in the schedule will be updated from the
        other schedule. Additional columns will be ignored.

        """

    def test_reschedule_required(self):
        """Test the reschedule_required method of the schedule.

        The method should return True on the first call since all
        values are np.nan.

        After updating the schedule (and advancing the time), the
        method should return False

        """

        schedule = Schedule(900, 2, self.now_dt)
        schedule.init()

        self.assertTrue(schedule.reschedule_required())

        schedule.update(self.other)
        # schedule.now_dt += timedelta(seconds=schedule.step_size)

        self.assertFalse(schedule.reschedule_required())

    def test_json(self):
        """Test de/serialization from and to json."""
        schedule = Schedule(
            900,
            2,
            self.now_dt,
            p_name="p_mw",
            q_name="q_mvar",
        )
        schedule.init()

        steps = 8
        other = pd.DataFrame(
            data=np.array(
                [
                    np.ones(steps),
                    np.zeros(steps),
                    np.ones(steps),
                    np.zeros(steps),
                ]
            ).transpose(),
            columns=["p_set_mw", "q_set_mvar", "p_mw", "q_mvar"],
            index=schedule._data.index,
        )
        schedule.update(other)

        schedule_json = schedule.to_json()

        schedule2 = Schedule()
        schedule2.from_json(schedule_json)

        for now_dt in schedule._data.index:
            for col in schedule._data.columns:
                self.assertEqual(
                    schedule.get(now_dt, col), schedule2.get(now_dt, col)
                )

        self.assertEqual(schedule.step_size, schedule2.step_size)
        self.assertEqual(schedule.now_dt, schedule2.now_dt)
        self.assertEqual(schedule.p_name, schedule2.p_name)
        self.assertEqual(schedule.q_name, schedule2.q_name)
        self.assertEqual(schedule.p_set_name, schedule2.p_set_name)
        self.assertEqual(schedule.q_set_name, schedule2.q_set_name)

    def test_from_dataframe(self):
        """Test the from_dataframe method of the schedule."""

    def test_update_row(self):
        schedule = Schedule(900, 2, self.now_dt)
        schedule.init()

        new_dt = self.now_dt + timedelta(seconds=900)

        schedule.update_row(new_dt, 30, 20, 25, 18)
        self.assertEqual(schedule.get(new_dt, "p_set_kw"), 30)
        self.assertEqual(schedule.get(new_dt, "q_set_kvar"), 20)
        self.assertEqual(schedule.get(new_dt, "p_kw"), 25)
        self.assertEqual(schedule.get(new_dt, "q_kvar"), 18)

        self.assertIsInstance(schedule._data.index, pd.DatetimeIndex)

    def test_update_entry(self):
        pass


if __name__ == "__main__":
    unittest.main()

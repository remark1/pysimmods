Schedule Model
==============

The schedule model is a wrapper for any *pysimmods* model (except for the utility models).
It allows models to follow a certain schedule other than the default schedule, which is hard-coded in the models' config.

This page provides examples in combination with the *Battery* model and provides detailled explanation of each of the examples' steps.
The code snippets will contain only the relevant parts. 
The full working example will be shown at the bottom of this page.

Instantiation
-------------

The process of creating an instance of the schedule model is straightfoward. 
All you have to do is to provide a model, which should be wrapped.

.. code-block:: python

    # First, create an instance of a battery model
    battery = Battery(*battery_presets(5))

    # Second, pass the battery to the schedule model
    smodel = ScheduleModel(
        battery, 
        unit="kw", 
        prioritize_setpoint=False,
    )

The function ``battery_presets`` is provided by the *batterysim* submodule and returns a tuple of two dictionaries.
The first dictionary contains parameters (i.e., static configuration) and the second dictionary contains values to initialize the internal state (i.e., dynamic configuration).
The ``*`` operator unpacks the tuple and passes them as two separate arguments to the battery constructor.

The schedule model itself only requires the model, which is the battery in this example. 
The other arguments are optional and displayed with their default value.
Nevertheless, they will be explained now.

unit
  The unit in which the schedule model should operate. 
  Default is ``"kw"`` for kilo watt and this is the value that all models of *pysimmods* use internally.
  However, changing ``unit`` to ``"w"`` or ``"mw"`` will force the schedule model to interpret every incoming value to be in that unit and it will also convert the models' outputs back to this unit.
  Although the argument is called ``"kw"``, this is also applied for reactive power accordingly.

prioritize_setpoint
  By default, the schedule model will prioritize values in a schedule higher than individual setpoints in a certain step.
  Setting ``prioritize_setpoint=True`` will force the schedule model to ignore setpoints from a schedule when a normal setpoint is provided in a certain step.

Regular Stepping
----------------

If no schedule is provided, the schedule model can be used just as the underlying model.
We will now simulate the battery for day of simulated time and plot the power output as well as the state of charge.

.. code-block:: python

    now_dt = datetime(2021, 6, 8, 14, 0, 0, tzinfo=timezone.utc)

    for i in range(steps):
        smodel.set_step_size(step_size)
        smodel.set_now_dt(now_dt)
        smodel.step()

        power[i] = smodel.get_p_kw()
        charge[i] = smodel.state.soc_percent

        now_dt += timedelta(seconds=step_size)

First, a ``datetime`` object is required, which allows the battery model to determine the correct value from the default schedule.
Next, ``steps=96`` simulation steps will be performed with ``step_size=900``.
After the models' step, we store the power value in a list ``power`` and the state of charge in a list ``charge``.
Since ``soc_percent`` is a model-specific value, we need to directly access the ``state`` object of the model.

.. image:: battery_only.png
    :width: 800

The darker line represents the default schedule for the battery.
We see that whenever the active power goes up, the state of charge of the battery increases as well.
If the active power is in the negative area, the battery discharges.

Adding a Schedule
-----------------

Now, we will send a schedule to the model and see how this affects the output in another day of simulation.
First, a schedule needs to be created, which can be done with *pandas*.

.. code-block:: python

    schedule = pd.DataFrame(
        columns=["target"],
        index=pd.date_range(
            now_dt + timedelta(hours=1),
            now_dt + timedelta(hours=24),
            freq=f"{step_size*3}S",
        ),
    )
    schedule.p_set_kw = 4.0

The schedule will start in hour into the future and will then be present for the rest of the (simulated) day.
However, we want the schedule to only be present each third step and, therefore, the ``freq`` argument is set to 3 times the ``step_size``.
The schedule data frame will look like this (``shape=(31, 1)``):

========================= ========
        (datetime index)  p_set_kw
========================= ========
2021-06-09 15:00:00+00:00   4.0
2021-06-09 15:45:00+00:00   4.0
2021-06-09 16:30:00+00:00   4.0
 ...                       ...
2021-06-09 12:45:00+00:00   4.0
2021-06-09 13:30:00+00:00   4.0
========================= ========

Next, we will pass the schedule to the model and start the simulation another time.

.. code-block:: python

    smodel.update_schedule(schedule)

    for i in range(steps):
       # Like above

And plotting the results:

.. image:: battery_schedule.png
    :width: 800

In contrast to the normal operation, we see that every third step the power output of the battery goes up to 4 kW.
In every other step, the default schedule is used.
However, between steps 30 and 40 a change happens: the battery is full.
As consequence, the power output stays at 0 and only at the end, where the default schedule discharges the battery.

Full Code for the Example
-------------------------

.. code-block:: python

    from datetime import datetime, timedelta, timezone

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from pysimmods.buffer.batterysim.battery import Battery
    from pysimmods.buffer.batterysim.presets import battery_presets
    from pysimmods.other.flexibility.schedule_model import ScheduleModel


    def main():

        # First, create an instance of a battery model
        battery = Battery(*battery_presets(5))

        # Second, pass the battery to the schedule model
        smodel = ScheduleModel(battery, use_absolute_setpoints=True)
        hour = 0
        now_dt = datetime(2021, 6, 8, hour, 0, 0, tzinfo=timezone.utc)

        step_size = 900
        steps = 96
        index = np.arange(steps)
        power = np.zeros(steps)
        charge = np.zeros(steps)

        # Simulate for one day
        for i in range(steps):
            smodel.set_step_size(step_size)
            smodel.set_now_dt(now_dt)
            smodel.step()

            power[i] = smodel.get_p_kw()
            charge[i] = smodel.state.soc_percent

            now_dt += timedelta(seconds=step_size)

        # Get the default schedule, hourly shifted to match the timestamp
        ds = np.array(smodel.config.default_p_schedule)
        if hour > 0:
            default_schedule = np.zeros_like(ds)
            default_schedule[:-hour] = ds[hour:]
            default_schedule[-hour:] = ds[:hour]
        else:
            default_schedule = ds

        # Plot the results
        _, axes = plt.subplots(2, figsize=(6, 6))

        axes[0].bar(index, power, color="lightgreen")
        axes[0].plot(
            index,
            default_schedule.repeat(3600 // step_size),
            color="darkgreen",
        )
        axes[0].set_ylabel("active power [kW]")

        axes[1].plot(charge, color="red")
        axes[1].set_xlabel("steps (15-minutes)")
        axes[1].set_ylabel("state of charge [%]")
        plt.savefig("battery_only.png", dpi=300, bbox_inches="tight")
        plt.close()

        # Create a schedule for the battery
        schedule = pd.DataFrame(
            columns=["p_set_kw"],
            index=pd.date_range(
                now_dt + timedelta(hours=1),
                now_dt + timedelta(hours=24),
                freq=f"{step_size*3}S",
            ),
        )
        schedule.p_set_kw = 4

        # Pass the schedule to the battery
        smodel.update_schedule(schedule)

        power2 = np.zeros(steps)
        charge2 = np.zeros(steps)

        # Simulate another day
        for i in range(steps):
            smodel.set_step_size(step_size)
            smodel.set_now_dt(now_dt)
            smodel.step()

            power2[i] = smodel.get_p_kw()
            charge2[i] = smodel.state.soc_percent

            now_dt += timedelta(seconds=step_size)

        # Plot again
        _, axes = plt.subplots(2, figsize=(6, 6))

        axes[0].bar(
            index,
            [0, 0, 0] + ([0, 4, 0] * 31),
            edgecolor="orange",
            fc=(0, 0, 0, 0),
        )
        axes[0].bar(index, power2, color="lightgreen")
        axes[0].plot(
            index,
            default_schedule.repeat(3600 // step_size),
            color="darkgreen",
        )

        axes[0].set_ylabel("active power [kW]")

        axes[1].plot(charge2, color="red")
        axes[1].set_xlabel("steps (15-minutes)")
        axes[1].set_ylabel("state of charge [%]")
        plt.savefig("battery_schedule.png", dpi=300, bbox_inches="tight")
        plt.close()


    if __name__ == "__main__":
        main()

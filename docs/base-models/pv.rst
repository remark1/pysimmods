Photovoltaic Power Plant
========================

A PV plant consists of one or more modules, but the model treats all modules as one big module. 
The impact of the temperature of the PV module on efficiency is considered.
Therefore, the temperature of the PV plant is explicitly modelled.

The PV modules are considered to be homogeneous bodies, which are specified by their mass per unit area and heat capacity.
To model the temperature of the PV modules heat flows between the PV modules and environment are calculated in each simulation step. 
The PV modules are heated by the share of sun radiation, which is not reflected by the surface of the PV modules and which is not converted into electric energy.
At the same time, the PV plant releases heat to the ambient air.
This heat flow is considered to be proportional to the temperature gradient between air temperature and module temperature.
The corresponding proportionality factor is the heat transmission coefficient between air and module. 
Wind speed dependency of this coefficient is not taken into account.

This page provides examples on how to use the PVPlantSystem model, which also includes an inverter for reactive power.
The code snippets during the examples only show the most important parts.
The full code of the example will be shown at the bottomt of this page.


Instantiation
-------------

You have to provide the two dictionaries ``params`` and ``inits``.
The dict ``params`` provides the parameters for the PV plant model and should at least look like this

.. code-block:: python

    params = {
        'a_m2': 15.0,
        'eta_percent': 25,
    }

The parameter ``a_m2`` specifies the overall area of the PV modules in square meter. 
The parameter ``eta_percent`` is optional and specifies the efficiency of the PV plant.
The peak power of the PV plant depends directly on the size of the Surface and the efficiency and is defined as

.. code-block:: python

    p_peak_kw = eta_percent/100 * a_m2

The dict ``inits`` provides the initial values for the state variables.
The PV plant model has one state variable that must be specified when the model is initialized.
It is ``t_module_deg_celsius`` (indicates the temperature of the PV modules in °C):

.. code-block:: python
    
    inits = {'t_module_deg_celsius': 25}

If you want to use the basic PV plant, pass those dictionaries to the model:

.. code-block:: python

    pv = PhotovoltaicPowerPlant(params, inits)

However, for the rest of this page we will use the PVPlantSystem, which includes an inverter. 
Some additional parameters and initial values are needed for the inverter.
Both sets of parameters and initial values should be packed into one dictionary each and then passed to the PVSystem model:

.. code-block:: python
    
    p_peak_kw = 9.0
    eta = 0.25
    cos_phi = 0.95

    params = {
        "pv": {
            "a_m2": p_peak_kw / eta, 
            "eta_percent": eta * 100.0,
        },
        "inverter": {
            "sn_kva": p_peak_kw / cos_phi,
            "q_control": "prioritize_p",
            "cos_phi": cos_phi,
            "inverter_mode": "capacitive",
        },
        "sign_convention": "active",
    }

    inits = {
        "pv": {"t_module_deg_celsius": 5.0},
        "inverter": None,
    }

    pvsys = PVPlantSystem(params, inits)

There also exists a preset function, which uses defaults and includes most of the code above:

.. code-block:: python

    pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9.0, cos_phi=0.95))

Parameters of the PV Plant
--------------------------

tbd

Simulating a Day
----------------

The PV plant requires weather data to perform the calculations.
In the source code, you will find an example weather data set (tests/fixtures/weather-time-series.csv) [#]_
If you download the data set, put it next to your python script and define a variable with the path to that file:

.. code-block:: python

    WD_PATH = os.path.abspath(
        os.path.join(__file__, "..", "weather-time-series.csv")
    )

    # Load the data with pandas
    wd = pandas.read_csv(WD_PATH, index_col=0)

The simulation loop can be defined like the following:

.. code-block:: python

    now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)
    step_size = 900

    for i in range(96):
        # Calculate weather index from timestamp
        widx = datetime_to_index(now_dt)
        
        # Provide necessary inputs
        pvsys.set_step_size(step_size)
        pvsys.set_now_dt(now_dt)
        pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
        pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
        pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

        pvsys.step()

        # Collect the ouputs
        p_kws[i] = pvsys.get_p_kw()
        q_kvars[i] = pvsys.get_q_kvar()
        cos_phis[i] = pvsys.get_cos_phi()
        t_modules[i] = pvsys.state.t_module_deg_celsius

        now_dt += timedelta(seconds=step_size)

``p_kws`` and the others are numpy arrays of length 96, which we can now use to plot the results:

.. image:: pvsys_only.png
    :width: 800

Since we gave no input to the PV plant, it will always output as much power as possible, dependend on the current weather situation.

.. [#] The data set is based on weather data from the DWD (German weather service). 
       The data was randomized and interpolated to have 15 minute resolution. 
       The original data set has hourly resolution.

Active Power Request
--------------------

Next, we want to see the behavior if set points for active power are provided. 
The simulation loop is defined as above.
However, just before the call of ``pvsys.step()``, we define a static active power request:

.. code-block:: python

    if 50 < i 60:
        pvsys.set_p_kw(4)
    
    pvsys.step()

Between the steps 50 and 60, the active power should not rise above 4 kW. 
The results looks like:

.. image:: pvsys_active_power_request.png
    :width: 800

We can see, that the absolute value of reactive power also goes down and the cos phi remains constant.

Reactive Power Request
----------------------

Finally, we want to see the behavior if set points for reactive power are provided.
Since a request to change reactive power does not come without reason, the inverter of the PV plant should ignore the cos phi and set the reactive as near as possible to the setpoint.
However, since the apparent nominal power is limited, the reactive power setpoint might always be feasible.
To show this behavior, we define two different reactive power requests just before the ``step()`` call:

.. code-block:: python

    if 25 < i < 35:
        pvsys.set_q_kvar(-0.5)
    if 40 < i < 50:
        pvsys.set_q_kvar(9)

    pvsys.step()

The result looks like:

.. image:: pvsys_reactive_power_request.png
    :width: 800

Active power does not change but we can clearly see the ``q_set_kvar=-0.5`` request and the impact on the cos phi.
We also see the ``q_set_kvar=9`` request, which is not totally feasible because of the active power peaks at the same time.

If we pass ``q_control="prioritize_q"`` to the preset function (or place it in the inverter params), this behavior changes.
In that case, active power is reduced as much as possible to provide the reactive power setpoint.

.. image:: pvsys_reactive_power_request2.png
    :width: 800

Full Code for the Example
-------------------------

.. code-block:: python

    import os
    from datetime import datetime, timedelta, timezone

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from pysimmods.generator.pvsystemsim.presets import pv_preset
    from pysimmods.generator.pvsystemsim.pvpsystem import PVPlantSystem

    T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
    BH = "WeatherCurrent__0___bh_w_per_m2"
    DH = "WeatherCurrent__0___dh_w_per_m2"

    # Set this to point to the csv file
    WD_PATH = os.path.abspath(
        os.path.join(__file__, "..", "weather-time-series.csv")
    )


    def pvsys_only():
        wd = pd.read_csv(WD_PATH, index_col=0)

        pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9, cos_phi=0.95))

        now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)

        steps = 96
        step_size = 900
        index = np.arange(steps)
        p_kws = np.zeros(steps)
        q_kvars = np.zeros(steps)
        cos_phis = np.zeros(steps)
        t_modules = np.zeros(steps)

        for i in range(steps):
            widx = datetime_to_index(now_dt)
            pvsys.set_step_size(step_size)
            pvsys.set_now_dt(now_dt)
            pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
            pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
            pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

            pvsys.step()

            p_kws[i] = pvsys.get_p_kw()
            q_kvars[i] = pvsys.get_q_kvar()
            cos_phis[i] = pvsys.get_cos_phi()
            t_modules[i] = pvsys.state.t_module_deg_celsius

            now_dt += timedelta(seconds=step_size)

        # Plot the results
        plot_results(
            index,
            p_kws,
            q_kvars,
            cos_phis,
            t_modules,
            "pvsys_only.png",
        )


    def active_power_request():
        wd = pd.read_csv(WD_PATH, index_col=0)

        pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9, cos_phi=0.95))

        now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)

        steps = 96
        step_size = 900
        index = np.arange(steps)
        p_kws = np.zeros(steps)
        q_kvars = np.zeros(steps)
        cos_phis = np.zeros(steps)
        t_modules = np.zeros(steps)

        for i in range(steps):
            widx = datetime_to_index(now_dt)
            pvsys.set_step_size(step_size)
            pvsys.set_now_dt(now_dt)
            pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
            pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
            pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

            if 50 < i < 60:
                pvsys.set_p_kw(4)
            pvsys.step()

            p_kws[i] = pvsys.get_p_kw()
            q_kvars[i] = pvsys.get_q_kvar()
            cos_phis[i] = pvsys.get_cos_phi()
            t_modules[i] = pvsys.state.t_module_deg_celsius

            now_dt += timedelta(seconds=step_size)

        # Plot the results
        plot_results(
            index,
            p_kws,
            q_kvars,
            cos_phis,
            t_modules,
            "pvsys_active_power_request.png",
        )


    def reactive_power_request():
        wd = pd.read_csv(WD_PATH, index_col=0)

        pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9, cos_phi=0.95))

        now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)
        steps = 96
        step_size = 900

        index = np.arange(steps)
        p_kws = np.zeros(steps)
        q_kvars = np.zeros(steps)
        cos_phis = np.zeros(steps)
        t_modules = np.zeros(steps)

        for i in range(steps):
            widx = datetime_to_index(now_dt)
            pvsys.set_step_size(step_size)
            pvsys.set_now_dt(now_dt)
            pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
            pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
            pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

            if 25 < i < 35:
                pvsys.set_q_kvar(-0.5)

            if 40 < i < 50:
                pvsys.set_q_kvar(9)
            pvsys.step()

            p_kws[i] = pvsys.get_p_kw()
            q_kvars[i] = pvsys.get_q_kvar()
            cos_phis[i] = pvsys.get_cos_phi()
            t_modules[i] = pvsys.state.t_module_deg_celsius

            now_dt += timedelta(seconds=step_size)

        # Plot the results
        plot_results(
            index,
            p_kws,
            q_kvars,
            cos_phis,
            t_modules,
            "pvsys_reactive_power_request.png",
        )


    def datetime_to_index(
        dt: datetime, step_size: int = 900, steps_per_year: int = 35135
    ) -> int:
        dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
        dif = dif.total_seconds()
        idx = int(dif // step_size) % steps_per_year

        return idx


    def plot_results(index, p_kws, q_kvars, cos_phis, t_modules, filename):
        _, axes = plt.subplots(2, 2, figsize=(12, 6))

        axes[0][0].bar(index, p_kws, color="green")
        axes[0][0].set_ylabel("active power [kW]")
        axes[1][0].bar(index, q_kvars, color="blue")
        axes[1][0].set_ylabel("reactive power [kVAr]")

        axes[0][1].plot(cos_phis, color="black")
        axes[0][1].set_ylabel("cos phi")
        axes[1][1].plot(t_modules, color="orange")
        axes[1][1].set_ylabel("module temperature [°C]")

        plt.savefig(filename, dpi=300, bbox_inches="tight")
        plt.close()


    if __name__ == "__main__":
        pvsys_only()
        active_power_request()
        reactive_power_request()

Heat Ventilation and Air Conditioning
=====================================

tbd

.. image:: hvac_only.png
    :width: 800

.. image:: hvac_medium_power.png
    :width: 800

.. image:: hvac_random_power.png
    :width: 800

Full Code for the Example
-------------------------

.. code-block:: python

    import os
    from datetime import datetime, timedelta, timezone

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from pysimmods.consumer.hvacsim.hvac import HVAC
    from pysimmods.consumer.hvacsim.presets import hvac_preset

    T_AIR = "WeatherCurrent__0___t_air_deg_celsius"

    WD_PATH = os.path.abspath(
        os.path.join(__file__, "..", "..", "fixtures", "weather-time-series.csv")
    )


    def main():
        wd = pd.read_csv(WD_PATH, index_col=0)

        hvac = HVAC(*hvac_preset(1279))
        now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

        steps = 96 * 4
        step_size = 900
        index = np.arange(steps)
        p_kws = np.zeros(steps)
        t_storages = np.zeros(steps)

        for i in range(steps):
            widx = datetime_to_index(now_dt)
            hvac.set_step_size(step_size)
            hvac.set_now_dt(now_dt)
            hvac.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]

            hvac.step()

            p_kws[i] = hvac.get_p_kw()
            t_storages[i] = hvac.state.theta_t_deg_celsius

            now_dt += timedelta(seconds=step_size)

        plot(index, p_kws, t_storages, "hvac_only.png")

        steps = 96 * 4
        step_size = 900
        index = np.arange(steps)
        p_kws = np.zeros(steps)
        t_storages = np.zeros(steps)

        for i in range(steps):
            widx = datetime_to_index(now_dt)
            hvac.set_step_size(step_size)
            hvac.set_now_dt(now_dt)
            hvac.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
            hvac.set_p_kw(600)
            hvac.step()

            p_kws[i] = hvac.get_p_kw()
            t_storages[i] = hvac.state.theta_t_deg_celsius

            now_dt += timedelta(seconds=step_size)

        plot(index, p_kws, t_storages, "hvac_medium_power.png")

        steps = 96 * 4
        step_size = 900
        index = np.arange(steps)
        p_kws = np.zeros(steps)
        t_storages = np.zeros(steps)

        for i in range(steps):
            widx = datetime_to_index(now_dt)
            hvac.set_step_size(step_size)
            hvac.set_now_dt(now_dt)
            hvac.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
            hvac.set_p_kw(np.random.randint(0, 1279))
            hvac.step()

            p_kws[i] = hvac.get_p_kw()
            t_storages[i] = hvac.state.theta_t_deg_celsius

            now_dt += timedelta(seconds=step_size)

        plot(index, p_kws, t_storages, "hvac_random_power.png")


    def plot(index, p_kws, t_storages, filename):
        # Plot the results
        _, axes = plt.subplots(2, figsize=(6, 6))

        axes[0].bar(index, p_kws, color="green")
        axes[0].set_ylabel("active power [kW]")

        axes[1].plot(t_storages, color="red")
        axes[1].set_ylabel("storage temperatore [°C]")
        axes[1].set_xlabel("steps (15-minutes)")
        plt.savefig(filename, dpi=300, bbox_inches="tight")
        plt.close()


    def datetime_to_index(
        dt: datetime, step_size: int = 900, steps_per_year: int = 35135
    ) -> int:
        dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
        dif = dif.total_seconds()
        idx = int(dif // step_size) % steps_per_year

        return idx


    if __name__ == "__main__":
        main()
